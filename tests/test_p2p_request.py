import os
import random

import pytest

from p2psec.rps.p2p.p2p_protocol import (
    P2PProtocol, Request, RequestType, RPSMsgFixedSize)


def test_request_size():
    req = Request()

    assert req.size == RPSMsgFixedSize.REQUEST
    assert req.request_struct.size == RPSMsgFixedSize.REQUEST

    rn = random.uniform(5, 50)

    req = Request(size=rn)
    assert req.size == rn


def test_request_pack_warns():
    req = Request()

    with pytest.warns(UserWarning):
        req.pack()

    req.rtype = max(RequestType) + 1
    with pytest.warns(UserWarning):
        req.pack()


def test_request_nonce():
    req = Request()
    assert req._nonce is None
    assert req.nonce is not None
    assert req._nonce is not None

    nonce = os.urandom(8)
    req = Request(nonce=nonce)
    assert req.nonce == nonce


def test_request_pack_unpack():
    req = Request(request_type=RequestType.PUSH)
    packed = req.pack()

    req2 = Request()
    req2.unpack(packed, offset=0)

    assert req.size == req2.size
    assert req.rtype == req2.rtype
    assert req.nonce == req2.nonce


def test_request_unpack():
    nonce = os.urandom(8)
    packed = (RPSMsgFixedSize.REQUEST.to_bytes(2, "big", signed=False) +
              bytes(1) +
              RequestType.PULL.to_bytes(1, "big", signed=False) +
              nonce)

    req = Request()
    req.unpack(packed, offset=0)

    assert req.size == RPSMsgFixedSize.REQUEST
    assert req.rtype == RequestType.PULL
    assert req.nonce == nonce


def test_request_pack():
    nonce = os.urandom(8)
    packed = (RPSMsgFixedSize.REQUEST.to_bytes(2, "big", signed=False) +
              bytes(1) +
              RequestType.PUSH.to_bytes(1, "big", signed=False) +
              nonce)

    req = Request(size=RPSMsgFixedSize.REQUEST, request_type=RequestType.PUSH,
                  nonce=nonce)

    assert packed == req.pack()


def test_request_prints():
    nonce = os.urandom(8)
    req = Request(request_type=RequestType.PUSH, nonce=nonce)

    _repr = repr(req)
    _str = str(req)

    assert str(int(RPSMsgFixedSize.REQUEST)) in _repr
    assert str(int(RPSMsgFixedSize.REQUEST)) in _str

    assert RequestType.PUSH.name in _repr
    assert RequestType.PUSH.name in _str

    assert nonce.hex() in _repr
    assert nonce.hex() in _str
