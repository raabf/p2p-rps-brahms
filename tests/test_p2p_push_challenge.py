from os import urandom

from p2psec.rps.p2p.p2p_push import PushChallenge


def test_p2p_push_challenge():
    nonce = urandom(8)
    resource = urandom(8)
    difficulty = 20
    size = 21

    pc = PushChallenge(nonce=nonce, resource=resource, difficulty=difficulty)

    assert pc.nonce == nonce
    assert pc.resource == resource
    assert pc.difficulty == difficulty
    assert pc.size == size


def test_p2p_push_challenge_pack():
    nonce = urandom(8)
    resource = urandom(8)
    difficulty = 20
    size = 21

    push_challenge_header = (size.to_bytes(2, "big", signed=False) + bytes(2) + nonce)

    packed = push_challenge_header + resource + difficulty.to_bytes(1, "big", signed=False)

    pc = PushChallenge(nonce=nonce, resource=resource, difficulty=difficulty)
    pc_packed = pc.pack()

    assert pc_packed == packed


def test_p2p_push_challenge_unpack():
    nonce = urandom(8)
    resource = urandom(8)
    difficulty = 20
    size = 21

    push_challenge_header = (size.to_bytes(2, "big", signed=False) + bytes(2) + nonce)

    packed = push_challenge_header + resource + difficulty.to_bytes(1, "big", signed=False)

    pc = PushChallenge()
    pc.unpack(packed, offset=0)

    assert pc.size == size
    assert pc.nonce == nonce
    assert pc.resource == resource
    assert pc.difficulty == difficulty


def test_p2p_push_challenge_prints():
    nonce = urandom(8)
    resource = urandom(8)
    difficulty = 20

    pc = PushChallenge(nonce=nonce, resource=resource, difficulty=difficulty)

    repr_ = repr(pc)

    assert repr(pc.size) in repr_
    assert nonce.hex() in repr_
    assert resource.hex() in repr_
    assert repr(difficulty) in repr_

    str_ = str(pc)

    assert repr(pc.size) in str_
    assert nonce.hex() in str_
    assert resource.hex() in str_
    assert str(difficulty) in str_
