import os
import random

import pytest
import xxhash

from p2psec.rps.sampler import Sampler


def test_too_small_block_size():
    with pytest.raises(ValueError):
        Sampler(0)
    with pytest.raises(ValueError):
        Sampler(0, attribute="test")


def test_sampler_block_bytes():
    length = int(random.uniform(2, 5))
    sampler = Sampler(length)

    for elem in sampler:
        assert elem.sample is None

    depth = int(random.uniform(1, 7))
    for _ in range(depth):
        for elem in sampler:
            elem.next(os.urandom(42))
            assert isinstance(elem.sample, bytes)
            assert str(elem.no) in str(elem)
            assert repr(elem.no) in repr(elem)

    for elem in sampler:
        elem.init()
        assert elem.sample is None

    for elem in sampler:
        elem.next(os.urandom(42))
        assert isinstance(elem.sample, bytes)

    sampler.update_samplers([os.urandom(42) for _ in range(
        int(random.uniform(1, 7)))])
    for elem in sampler:
        assert isinstance(elem.sample, bytes)


def test_sampler_block_object():
    class Dummy(object):
        def __init__(self):
            self.test_attr = os.urandom(42)

    length = int(random.uniform(2, 5))
    sampler = Sampler(length, attribute="test_attr")

    for elem in sampler:
        assert elem.sample is None

    depth = int(random.uniform(1, 7))
    for _ in range(depth):
        for elem in sampler:
            elem.next(Dummy())
            assert isinstance(elem.sample, Dummy)
            assert str(elem.no) in str(elem)
            assert repr(elem.no) in repr(elem)

    for elem in sampler:
        elem.init()
        assert elem.sample is None

    for elem in sampler:
        elem.next(Dummy())
        assert isinstance(elem.sample, Dummy)

    sampler.update_samplers([Dummy() for _ in range(
        int(random.uniform(1, 7)))])
    for elem in sampler:
        assert isinstance(elem.sample, Dummy)


def test_sampler_hash():
    # this also checks for correct big-endian
    sampler = Sampler(1)
    sb = sampler[0]

    sample = os.urandom(44)

    ih = xxhash.xxh64(sample, seed=sb._seed).intdigest().to_bytes(8, "big")
    sbh = sb.hash(sample)

    assert ih == sbh
