import struct
from ipaddress import IPv4Address, IPv6Address
from os import urandom
from random import uniform

import pytest

from p2psec.msg_types import (
    MAX_MSG_SIZE, MIN_MSG_SIZE, MsgFixedSize, MsgMinSize, MsgType,
    RPSPeerFlags)
from p2psec.rps.api.api_protocol import (
    APIProtocol, NSEQuery, RPSPeer, RPSQuery)
from p2psec.rps.globals import Automatic
from p2psec.rps.peer import Peer


def asserts_rps_query(rpsq):
    assert rpsq.size == MsgFixedSize.RPS_QUERY
    assert rpsq.size >= MsgMinSize.RPS_QUERY
    assert rpsq.size < MAX_MSG_SIZE
    assert rpsq.api_type == MsgType.RPS_QUERY


def test_prints_rps_query():
    rpsq = RPSQuery()

    str_ = str(rpsq)
    assert MsgType.RPS_QUERY.name in str_
    assert str(MsgFixedSize.RPS_QUERY.value) in str_

    repr_ = repr(rpsq)
    assert RPSQuery.__name__ in repr_
    assert str(MsgFixedSize.RPS_QUERY.value) in repr_

    # testing another branch with different output because of not standart size

    size = int(uniform(4, 2 ** 16 - 1))

    with pytest.warns(UserWarning):
        rpsq = RPSQuery(size)

        str_ = str(rpsq)
    assert MsgType.RPS_QUERY.name in str_
    assert str(size) in str_

    with pytest.warns(UserWarning):
        repr_ = repr(rpsq)
    assert RPSQuery.__name__ in repr_
    assert MsgType.RPS_QUERY.name in repr_
    assert str(size) in repr_


def test_prints_rps_peer():
    size = int(uniform(0, 2 ** 16 - 1))
    der = urandom(8)
    port = int(uniform(0, 2 ** 16 - 1))
    ip4 = IPv4Address(urandom(4))

    with pytest.warns(UserWarning):
        rpsp = RPSPeer(size=size, port=port, ip=ip4, hostkey=der)

        str_ = str(rpsp)

    assert MsgType.RPS_PEER.name in str_
    assert str(size) in str_
    assert str(port) in str_
    assert str(ip4) in str_

    with pytest.warns(UserWarning):
        repr_ = repr(rpsp)

    assert RPSPeer.__name__ in repr_
    assert MsgType.RPS_PEER.name in repr_
    assert str(size) in repr_
    assert str(size) in repr_
    assert str(port) in repr_
    assert str(ip4) in repr_
    assert str(der.hex()) in repr_


def test_query_create():
    rpsq = RPSQuery()
    asserts_rps_query(rpsq)

    size = int(uniform(MIN_MSG_SIZE, MAX_MSG_SIZE))
    rpsq = RPSQuery(size)

    with pytest.warns(UserWarning):
        assert rpsq.size == size
        assert rpsq.size >= MsgMinSize.RPS_QUERY
        assert rpsq.size < MAX_MSG_SIZE

    assert rpsq.api_type == MsgType.RPS_QUERY


def test_query_pack():
    rpsq = RPSQuery()
    packed = rpsq.pack()

    assert packed == (0x00040000 + MsgType.RPS_QUERY).to_bytes(4, "big")


def test_query_unpack():
    packed = (0x00040000 + MsgType.RPS_QUERY).to_bytes(4, "big")

    rpsq = APIProtocol.unpack_factory(packed)
    asserts_rps_query(rpsq)


def test_size_calc_ipv4():
    ip = IPv4Address(urandom(4))

    der0 = bytes(0)
    rpsp0 = RPSPeer(size=Automatic, port=int(uniform(1, 2 ** 16 - 1)),
                    ipv6=Automatic, ip=ip, hostkey=der0)

    assert rpsp0.size == 4 + 4 + 4 + 0

    der = urandom(60)
    rpsp = RPSPeer(size=Automatic, port=int(uniform(1, 2 ** 16 - 1)),
                   ipv6=Automatic, ip=ip, hostkey=der)

    assert rpsp.size == 4 + 4 + 4 + len(der)

    new_size = rpsp.size + 1
    rpsp.size = new_size
    with pytest.warns(UserWarning):
        assert rpsp.size == new_size


def test_rps_peer_size_empty():
    rpsp = RPSPeer()
    with pytest.warns(UserWarning):
        assert rpsp.size is None

    assert rpsp.hostkey is None
    assert rpsp.ip is None
    assert rpsp.port is None

    assert rpsp.peer is None


def test_rps_peer_size_calc_ipv6():
    ip = IPv6Address(urandom(16))

    der0 = bytes(0)
    rpsp0 = RPSPeer(size=Automatic, port=int(uniform(1, 2 ** 16 - 1)),
                    ipv6=Automatic, ip=ip, hostkey=der0)

    assert rpsp0.size == 4 + 4 + 16 + 0

    der = urandom(60)
    rpsp = RPSPeer(size=Automatic, port=int(uniform(1, 2 ** 16 - 1)),
                   ipv6=Automatic, ip=ip, hostkey=der)

    assert rpsp.size == 4 + 4 + 16 + len(der)

    new_size = rpsp.size + 1
    rpsp.size = new_size
    with pytest.warns(UserWarning):
        assert rpsp.size == new_size


def test_rps_peer_ipv6():
    ip = IPv4Address(urandom(4))
    der = urandom(60)

    rpsp = RPSPeer(size=Automatic, port=42, ipv6=Automatic, ip=ip, hostkey=der)
    assert not rpsp.ipv6

    rpsp.ip = IPv6Address(urandom(16))
    assert rpsp.ipv6

    with pytest.warns(UserWarning):
        rpsp.ipv6 = False
        assert not rpsp.ipv6
        assert RPSPeerFlags.ipv6 not in rpsp.flags

    with pytest.warns(UserWarning):
        rpsp.ip = ip
        rpsp.ipv6 = True
        assert rpsp.ipv6
        assert RPSPeerFlags.ipv6 in rpsp.flags

    with pytest.warns(UserWarning):
        rpsp.ipv6 = None
        rpsp.ip = None
        assert rpsp.ipv6 is None


def test_rps_peer_pack():
    der = urandom(8)
    port = int(uniform(0, 2 ** 16 - 1))
    ip4 = IPv4Address(urandom(4))
    packed = ((12 + len(der)).to_bytes(2, "big") +
              MsgType.RPS_PEER.to_bytes(2, "big") +
              port.to_bytes(2, "big") + (0x0000).to_bytes(2, "big") +
              ip4.packed + der)

    rpsp = RPSPeer(port=port, ip=ip4, hostkey=der)

    assert packed == rpsp.pack()

    ip6 = IPv6Address(urandom(16))
    rpsp2 = RPSPeer(size=24 + len(der), port=port, ipv6=True, ip=ip6,
                    hostkey=der)

    packed2 = ((24 + len(der)).to_bytes(2, "big") +
               MsgType.RPS_PEER.to_bytes(2, "big") +
               port.to_bytes(2, "big") + (0x0001).to_bytes(2, "big") +
               ip6.packed + der)

    assert packed2 == rpsp2.pack()


def test_rps_peer_unpack():
    der = urandom(8)
    port = int(uniform(0, 2 ** 16 - 1))
    ip4 = IPv4Address(urandom(4))
    packed = ((12 + len(der)).to_bytes(2, "big") +
              MsgType.RPS_PEER.to_bytes(2, "big") +
              port.to_bytes(2, "big") + (0x0000).to_bytes(2, "big") +
              ip4.packed + der)

    rpsp = APIProtocol.unpack_factory(packed)
    rpsp.unpack(packed, offset=4)

    assert isinstance(rpsp, RPSPeer)
    assert rpsp.port == port
    assert not rpsp.ipv6
    assert rpsp.size == 12 + len(der)
    assert rpsp.ip == ip4
    assert rpsp.hostkey == der
    assert RPSPeerFlags.ipv6 not in rpsp.flags

    ip6 = IPv6Address(urandom(16))
    random_extension = int(uniform(6, 42))

    packed = ((24 + len(der) + random_extension).to_bytes(2, "big") +
              MsgType.RPS_PEER.to_bytes(2, "big") +
              port.to_bytes(2, "big") + (0x0001).to_bytes(2, "big") +
              ip6.packed + der)

    rpsp = APIProtocol.unpack_factory(packed)
    rpsp.unpack(packed, offset=4)

    assert isinstance(rpsp, RPSPeer)
    assert rpsp.port == port
    assert rpsp.ipv6
    with pytest.warns(UserWarning):
        assert rpsp.size == 24 + len(der) + random_extension
    assert rpsp.ip == ip6
    assert rpsp.hostkey != der
    assert RPSPeerFlags.ipv6 in rpsp.flags


def test_rps_peer_property():
    oport = int(uniform(0, 2 ** 16 - 1))
    der = urandom(8)
    ipv6 = IPv6Address(urandom(16))
    peer = Peer(ip=ipv6, onion_port=oport, hostkey=der)

    rpsp = RPSPeer(peer=peer)

    # check take over of variables inside peer
    assert rpsp.ip == ipv6
    assert rpsp.hostkey == der
    assert rpsp.port == oport

    # check coorect contruction of peer property
    assert rpsp.peer.ip == ipv6
    assert rpsp.peer.hostkey == der
    assert rpsp.peer.onion_port == oport

    # check if ip, port, hostkey parameter overwrites the ones in peer
    oport2 = int(uniform(0, 2 ** 16 - 1))
    der2 = urandom(8)
    ipv62 = IPv6Address(urandom(16))

    rpsp = RPSPeer(peer=peer, ip=ipv62, port=oport2, hostkey=der2)

    assert rpsp.port == oport2
    assert rpsp.hostkey == der2
    assert rpsp.ip == ipv62

    # check empty RPSPeer
    rpsp = RPSPeer()
    assert rpsp.ip is None
    assert rpsp.hostkey is None
    assert rpsp.port is None
