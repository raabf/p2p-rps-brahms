from ipaddress import IPv4Address, IPv6Address
from os import urandom
from random import uniform

import pytest

from p2psec.rps.globals import Automatic
from p2psec.rps.p2p.p2p_push import PushReply
from p2psec.rps.p2p.peer_protocol import PeerProtocol


def random_peer_proto():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    der = urandom(42)

    if uniform(0, 1) < 0.5:
        ip = IPv4Address(urandom(4))
    else:
        ip = IPv6Address(urandom(16))

    peerp = PeerProtocol(hostkey=der, ip=ip, onion_port=oport,
                         rps_port=rport, size=Automatic, ipv=Automatic)

    return peerp


def test_p2p_push_reply():
    nonce = urandom(8)
    peer = random_peer_proto()
    answer = urandom(64)

    pr = PushReply(nonce=nonce, peer=peer, answer=answer)

    assert pr.nonce == nonce
    assert pr.answer == answer
    assert pr.size == 12 + peer.size + len(answer)


def test_p2p_push_reply_pack():
    nonce = urandom(8)
    peer = random_peer_proto()
    answer = urandom(64)
    size = 12 + peer.size + len(answer)

    push_reply_header = (size.to_bytes(2, "big", signed=False) + bytes(2) + nonce)

    packed = push_reply_header + peer.pack() + answer

    pr = PushReply(nonce=nonce, peer=peer, answer=answer)
    pr_packed = pr.pack()

    assert pr_packed == packed


def test_p2p_push_reply_unpack():
    nonce = urandom(8)
    peer = random_peer_proto()
    answer = urandom(64)
    size = 12 + peer.size + len(answer)

    push_reply_header = (size.to_bytes(2, "big", signed=False) + bytes(2) + nonce)

    packed = push_reply_header + peer.pack() + answer

    pr = PushReply()
    pr.unpack(packed, offset=0)

    assert pr.size == size
    assert pr.nonce == nonce
    assert pr.answer == answer
    assert pr.peer.hostkey == peer.hostkey
    assert pr.peer.ip == peer.ip


def test_p2p_push_reply_prints():
    nonce = urandom(8)
    peer = random_peer_proto()
    answer = urandom(64)

    pr = PushReply(nonce=nonce, peer=peer, answer=answer)

    repr_ = repr(pr)

    assert repr(pr.size) in repr_
    assert nonce.hex() in repr_
    assert answer.hex()[0:15] in repr_

    str_ = str(pr)

    assert repr(pr.size) in str_
    assert nonce.hex() in str_
    assert answer.hex()[0:15] in str_
