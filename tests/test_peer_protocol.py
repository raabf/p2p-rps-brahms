from ipaddress import IPv4Address, IPv6Address
from os import urandom
from random import uniform

import pytest

from p2psec.rps.globals import Automatic
from p2psec.rps.p2p.peer_protocol import PeerProtocol


def test_peer_proto_size():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    ipv4 = IPv4Address(urandom(4))
    ipv6 = IPv6Address(urandom(16))
    der = urandom(42)
    size4 = 8 + 4 + len(der)
    size6 = 8 + 16 + len(der)

    peerp = PeerProtocol(hostkey=der, ip=ipv4, onion_port=oport,
                         rps_port=rport, size=size4, ipv=4)

    assert peerp.onion_port == oport
    assert peerp.rps_port == rport
    assert peerp.ip == ipv4
    assert peerp.hostkey == der
    assert peerp.size == size4

    peerp.size = Automatic
    peerp.ipv = Automatic
    peerp.ip = ipv6

    assert peerp.ip == ipv6
    assert peerp.size == size6
    assert peerp.ipv == 6

    peerp.ip = "Wrong type"
    with pytest.raises(NotImplementedError):
        peerp.ipv


def test_peer_proto_size():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    ipv4 = IPv4Address(urandom(4))
    ipv6 = IPv6Address(urandom(16))
    der = urandom(42)
    size4 = 8 + 4 + len(der)
    size6 = 8 + 16 + len(der)

    peerp = PeerProtocol(hostkey=der, ip=ipv4, onion_port=oport,
                         rps_port=rport, size=size4, ipv=4)

    assert peerp.size == size4

    peerp = PeerProtocol(hostkey=der, ip=ipv4, onion_port=oport,
                         rps_port=rport, size=Automatic, ipv=Automatic)

    assert peerp.size == size4

    peerp = PeerProtocol(hostkey=der, ip=ipv6, onion_port=oport,
                         rps_port=rport, size=Automatic, ipv=Automatic)

    assert peerp.size == size6

    new_size = size6 + int(uniform(1, 42))
    peerp.size = new_size

    with pytest.warns(UserWarning):
        assert peerp.size == new_size

    peerp.ipv = 5
    assert peerp.ipv == 5

    with pytest.warns(UserWarning):
        peerp.size

    peerp.size = Automatic
    with pytest.warns(UserWarning):
        assert peerp.size is None


def test_peer_proto_pack_unfitting_buffer():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    der = urandom(42)

    # IPv4, manual
    ip = IPv4Address(urandom(4))
    random_extension = int(uniform(1,42))

    packed = ((8 + 4 + len(der)).to_bytes(2, "big", signed=False) +
              (4).to_bytes(1, "big", signed=False) + bytes(1) +
              rport.to_bytes(2, "big", signed=False) +
              oport.to_bytes(2, "big", signed=False) +
              ip.packed + der + bytes(random_extension)
              )

    buffer = bytearray(8 + 4 + len(der) + random_extension)

    peerp = PeerProtocol(hostkey=der, ip=ip, onion_port=oport,
                         rps_port=rport, size=(8 + 4 + len(der)), ipv=4)
    peerp.pack_into(buffer, offset=0)

    assert buffer == packed

    # test a too big hostkey

    buffer = bytearray(8 + 4 + len(der) + random_extension)
    peerp = PeerProtocol(hostkey=der + bytes(random_extension), ip=ip,
                         onion_port=oport,
                         rps_port=rport, size=(8 + 4 + len(der)), ipv=4)

    with pytest.warns(UserWarning):
        peerp.pack_into(buffer, offset=0)

    assert buffer != packed


def test_peer_proto_pack_ipv4():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    der = urandom(42)

    # IPv4, manual
    ip = IPv4Address(urandom(4))

    packed = ((8 + 4 + len(der)).to_bytes(2, "big", signed=False) +
              (4).to_bytes(1, "big", signed=False) + bytes(1) +
              rport.to_bytes(2, "big", signed=False) +
              oport.to_bytes(2, "big", signed=False) +
              ip.packed + der
              )

    peerp = PeerProtocol(hostkey=der, ip=ip, onion_port=oport,
                         rps_port=rport, size=(8 + 4 + len(der)), ipv=4)
    pp_packed = peerp.pack()

    assert pp_packed == packed


def test_peer_proto_pack_ipv6():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    der = urandom(42)

    # IPv6, Automatic
    ip = IPv6Address(urandom(16))

    packed = ((8 + 16 + len(der)).to_bytes(2, "big", signed=False) +
              (6).to_bytes(1, "big", signed=False) + bytes(1) +
              rport.to_bytes(2, "big", signed=False) +
              oport.to_bytes(2, "big", signed=False) +
              ip.packed + der
              )

    peerp = PeerProtocol(hostkey=der, ip=ip, onion_port=oport,
                         rps_port=rport, size=Automatic, ipv=Automatic)
    pp_packed = peerp.pack()

    assert pp_packed == packed

    buffer = bytearray(peerp.size)
    with pytest.raises(Exception):
        peerp.pack_into(buffer, offset=peerp.size - 2)


def test_peer_proto_unpack_ipv4():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    der = urandom(42)

    # IPv4, manual
    ip = IPv4Address(urandom(4))

    packed = ((8 + 4 + len(der)).to_bytes(2, "big", signed=False) +
              (4).to_bytes(1, "big", signed=False) + bytes(1) +
              rport.to_bytes(2, "big", signed=False) +
              oport.to_bytes(2, "big", signed=False) +
              ip.packed + der +
              bytes(5)  # too long buffer
              )

    peerp = PeerProtocol()
    peerp.unpack(packed)

    assert peerp.onion_port == oport
    assert peerp.rps_port == rport
    assert peerp.ip == ip
    assert peerp.ipv == 4
    assert peerp.size == (8 + 4 + len(der))
    assert peerp.hostkey == der


def test_peer_proto_unpack_ipv6():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    der = urandom(42)

    # IPv6, Automatic
    ip = IPv6Address(urandom(16))

    packed = ((8 + 16 + len(der)).to_bytes(2, "big", signed=False) +
              (6).to_bytes(1, "big", signed=False) + bytes(1) +
              rport.to_bytes(2, "big", signed=False) +
              oport.to_bytes(2, "big", signed=False) +
              ip.packed + der
              )

    peerp = PeerProtocol()

    peerp.unpack(packed)

    assert peerp.onion_port == oport
    assert peerp.rps_port == rport
    assert peerp.ip == ip
    assert peerp.ipv == 6
    assert peerp.size == (8 + 16 + len(der))
    assert peerp.hostkey == der


def test_peer_proto_pack_unpack_ipv4():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    der = urandom(42)

    ip = IPv4Address(urandom(4))

    peerp = PeerProtocol(hostkey=der, ip=ip, onion_port=oport,
                         rps_port=rport, size=Automatic, ipv=Automatic)

    packed = peerp.pack()

    unpacked = PeerProtocol()
    unpacked.unpack(packed)

    assert peerp.size == unpacked.size
    assert peerp.ipv == unpacked.ipv
    assert peerp.ip == unpacked.ip
    assert peerp.hostkey == unpacked.hostkey
    assert peerp.onion_port == unpacked.onion_port
    assert peerp.rps_port == unpacked.rps_port

    unpacked.clean()

    assert peerp.size == unpacked.size
    assert peerp.ipv == unpacked.ipv
    assert peerp.ip == unpacked.ip
    assert peerp.hostkey == unpacked.hostkey
    assert peerp.onion_port == unpacked.onion_port
    assert peerp.rps_port == unpacked.rps_port


def test_peer_proto_pack_unpack_ipv6():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    der = urandom(42)

    ip = IPv6Address(urandom(16))

    peerp = PeerProtocol(hostkey=der, ip=ip, onion_port=oport,
                         rps_port=rport, size=Automatic, ipv=Automatic)

    packed = peerp.pack()

    unpacked = PeerProtocol()
    unpacked.unpack(packed)

    assert peerp.size == unpacked.size
    assert peerp.ipv == unpacked.ipv
    assert peerp.ip == unpacked.ip
    assert peerp.hostkey == unpacked.hostkey
    assert peerp.onion_port == unpacked.onion_port
    assert peerp.rps_port == unpacked.rps_port

    unpacked.clean()

    assert peerp.size == unpacked.size
    assert peerp.ipv == unpacked.ipv
    assert peerp.ip == unpacked.ip
    assert peerp.hostkey == unpacked.hostkey
    assert peerp.onion_port == unpacked.onion_port
    assert peerp.rps_port == unpacked.rps_port

    unpacked.size = peerp.size  # sets size to a fixed value
    unpacked.hostkey = urandom(1)

    with pytest.warns(UserWarning):
        unpacked.clean()


def test_peer_proto_prints():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    der = urandom(42)

    ip = IPv4Address(urandom(4))

    peerp = PeerProtocol(hostkey=der, ip=ip, onion_port=oport,
                         rps_port=rport, size=Automatic, ipv=Automatic)

    str_ = str(peerp)

    assert str(peerp.size) in str_
    assert str(peerp.ipv) in str_
    assert str(peerp.ip) in str_
    assert str(peerp.onion_port) in str_
    assert str(peerp.rps_port) in str_

    repr_ = repr(peerp)

    assert repr(peerp.size) in repr_
    assert repr(peerp.ipv) in repr_
    assert str(peerp.ip) in repr_
    assert repr(peerp.onion_port) in repr_
    assert repr(peerp.rps_port) in repr_
