from p2psec.msg_types import (
    MAX_MSG_SIZE, MIN_MSG_SIZE, MsgFixedSize, MsgMinSize)


def test_msg_types_all_fixed_are_in_min():
    for ele in MsgFixedSize:
        min_ele = MsgMinSize[ele.name]
        assert min_ele.value == ele.value


def test_msg_types_min_max_size():
    for ele in MsgMinSize:
        assert ele.value >= MIN_MSG_SIZE
        assert ele.value < MAX_MSG_SIZE
        # implicitly MsgMinSize are also tested because of
        # test_msg_types_all_fixed_are_in_min()
