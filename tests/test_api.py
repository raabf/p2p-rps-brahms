import asyncio
import io
import os
import struct
from ipaddress import IPv4Address, IPv6Address

import pytest

from p2psec.msg_types import (MAX_MSG_SIZE, MIN_MSG_SIZE,
                              GossipValidationFlags, MsgFixedSize, MsgMinSize,
                              MsgType)
from p2psec.rps.api.api_protocol import (APIProtocol, GossipAnnounce,
                                         GossipNotification, GossipNotify,
                                         GossipValidation, NSEEstimate,
                                         NSEQuery, RPSQuery)
from p2psec.rps.api.server import APIServer
from p2psec.rps.brahms import Brahms
from p2psec.rps.globals import DERCert, Port, rps_config
from p2psec.rps.p2p.peer_protocol import PeerProtocol


def asserts_nse_query(nseq):
    assert nseq.size == MsgFixedSize.NSE_QUERY
    assert nseq.size >= MsgMinSize.NSE_QUERY
    assert nseq.size < MAX_MSG_SIZE
    assert nseq.api_type == MsgType.NSE_QUERY


def test_msg_type_uniqueness():
    for name, value in MsgType.__members__.items():
        elem = MsgType[name]
        elem2 = MsgType(elem.value)
        assert name is elem2.name


def test_size_exception():
    with pytest.raises(ValueError):
        RPSQuery(MIN_MSG_SIZE - 1)
    with pytest.raises(ValueError):
        RPSQuery(MAX_MSG_SIZE)


def test_size_exception():
    with pytest.raises(ValueError):
        NSEQuery(MIN_MSG_SIZE - 1)
    with pytest.raises(ValueError):
        NSEQuery(MAX_MSG_SIZE + 1)


def test_init_nse_query():
    nseq = NSEQuery(MsgFixedSize.NSE_QUERY)
    asserts_nse_query(nseq)


def test_nse_query_strings():
    nseq = NSEQuery()
    nseqr = repr(nseq)
    nseqs = str(nseq)
    assert MsgType.NSE_QUERY.name in nseqr
    assert MsgType.NSE_QUERY.name in nseqs
    assert str(MsgFixedSize.NSE_QUERY.value) in nseqr
    assert str(MsgFixedSize.NSE_QUERY.value) in nseqs


def test_pack_nse_query():
    nseq = NSEQuery(MsgFixedSize.NSE_QUERY)
    bytess = nseq.pack()

    assert len(bytess) == nseq.size

    packed_size = bytess[:2]
    packed_api_type = bytess[2:]

    assert packed_size == struct.pack("!H", nseq.size)
    assert packed_api_type == struct.pack("!H", nseq.api_type)
    assert packed_size + packed_api_type == bytess


def test_factory_nse_query():
    bytess = struct.pack(APIProtocol.size_type_struct.format, MsgFixedSize.NSE_QUERY,
                         MsgType.NSE_QUERY)

    nseq = APIProtocol.unpack_factory(bytess)
    asserts_nse_query(nseq)


def asserts_nse_estimate(nsee):
    assert nsee.size == MsgFixedSize.NSE_ESTIMATE
    assert nsee.size >= MsgMinSize.NSE_ESTIMATE
    assert nsee.size < MAX_MSG_SIZE
    assert nsee.api_type == MsgType.NSE_ESTIMATE
    assert nsee.estimate_peers >= 0
    assert nsee.estimate_std_dev >= 0


def test_init_nse_estimate():
    nsee = NSEEstimate(MsgFixedSize.NSE_ESTIMATE, 0, 0)
    asserts_nse_estimate(nsee)


def test_nse_estimate_strings():
    nsee = NSEEstimate(MsgFixedSize.NSE_ESTIMATE, 0, 0)

    nseer = repr(nsee)
    nsees = str(nsee)
    assert MsgType.NSE_ESTIMATE.name in nseer
    assert MsgType.NSE_ESTIMATE.name in nsees
    assert str(MsgFixedSize.NSE_ESTIMATE.value) in nseer
    assert str(MsgFixedSize.NSE_ESTIMATE.value) in nsees


def test_pack_nse_estimate():
    nsee = NSEEstimate(MsgFixedSize.NSE_ESTIMATE, 0, 0)
    bytess = nsee.pack()

    assert len(bytess) == nsee.size

    packed_size = bytess[:2]
    packed_api_type = bytess[2:4]
    packed_estimate_peers = bytess[4:8]
    packed_estimate_std_dev = bytess[8:]

    assert packed_size == struct.pack("!H", nsee.size)
    assert packed_api_type == struct.pack("!H", nsee.api_type)
    assert packed_estimate_peers == struct.pack("!I", nsee.estimate_peers)
    assert packed_estimate_std_dev == struct.pack("!I", nsee.estimate_std_dev)
    assert packed_size + packed_api_type + \
           packed_estimate_peers + packed_estimate_std_dev == bytess


def test_factory_nse_estimate():
    nsee = NSEEstimate(MsgFixedSize.NSE_ESTIMATE, 0, 0)
    bytess = nsee.pack()

    nsee = APIProtocol.unpack_factory(bytess)
    nsee.unpack(bytess, 4)
    asserts_nse_estimate(nsee)


def asserts_gossip_announce(gannounce):
    assert gannounce.size >= MsgMinSize.GOSSIP_ANNOUNCE
    assert gannounce.size < MAX_MSG_SIZE
    assert gannounce.api_type == MsgType.GOSSIP_ANNOUNCE


def test_init_gossip_announce():
    gannounce = GossipAnnounce(MsgMinSize.GOSSIP_ANNOUNCE + 1, 0, 0, 0, b'\x00')
    asserts_gossip_announce(gannounce)


def test_gossip_announce_strings():
    gannounce = GossipAnnounce(MsgMinSize.GOSSIP_ANNOUNCE + 1, 0, 0, 0, b'\x00')
    gannouncer = repr(gannounce)
    gannounces = str(gannounce)
    assert MsgType.GOSSIP_ANNOUNCE.name in gannouncer
    assert MsgType.GOSSIP_ANNOUNCE.name in gannounces
    assert str(MsgMinSize.GOSSIP_ANNOUNCE.value + 1) in gannouncer
    assert str(MsgMinSize.GOSSIP_ANNOUNCE.value + 1) in gannounces


def test_pack_gossip_announce():
    gannounce = GossipAnnounce(MsgMinSize.GOSSIP_ANNOUNCE + 1, 0, 0, 0, b'\x00')
    bytess = gannounce.pack()

    packed_size = bytess[:2]
    packed_api_type = bytess[2:4]
    packed_ttl = bytess[4:5]
    packed_reserved = bytess[5:6]
    packed_data_type = bytess[6:8]
    packed_data = bytess[8:]

    assert packed_size == struct.pack("!H", gannounce.size)
    assert packed_api_type == struct.pack("!H", gannounce.api_type)
    assert packed_ttl == struct.pack("!B", gannounce.ttl)
    assert packed_reserved == struct.pack("!B", gannounce.reserved)
    assert packed_data_type == struct.pack("!H", gannounce.data_type)
    assert packed_size + packed_api_type + packed_ttl + \
           packed_reserved + packed_data_type + packed_data == bytess


def test_factory_gossip_announce():
    gannounce = GossipAnnounce(MsgMinSize.GOSSIP_ANNOUNCE + 1, 0, 0, 0, b'\x00')
    bytess = gannounce.pack()

    gannounce = APIProtocol.unpack_factory(bytess)
    assert gannounce.api_type == MsgType.GOSSIP_ANNOUNCE


def asserts_gossip_notify(gnotify):
    assert gnotify.size == MsgFixedSize.GOSSIP_NOTIFY
    assert gnotify.size >= MsgMinSize.GOSSIP_NOTIFY
    assert gnotify.size < MAX_MSG_SIZE
    assert gnotify.api_type == MsgType.GOSSIP_NOTIFY


def test_init_gossip_notify():
    gnotify = GossipNotify(MsgFixedSize.GOSSIP_NOTIFY, 0, 0)
    asserts_gossip_notify(gnotify)


def test_gossip_notify_strings():
    gnotify = GossipNotify(MsgFixedSize.GOSSIP_NOTIFY, 0, 0)
    gnotifyr = repr(gnotify)
    gnotifys = str(gnotify)
    assert MsgType.GOSSIP_NOTIFY.name in gnotifyr
    assert MsgType.GOSSIP_NOTIFY.name in gnotifys
    assert str(MsgFixedSize.GOSSIP_NOTIFY.value) in gnotifyr
    assert str(MsgFixedSize.GOSSIP_NOTIFY.value) in gnotifys


def test_pack_gossip_notify():
    gnotify = GossipNotify(MsgFixedSize.GOSSIP_NOTIFY, 0, 0)
    bytess = gnotify.pack()

    assert len(bytess) == gnotify.size

    packed_size = bytess[:2]
    packed_api_type = bytess[2:4]
    packed_reserved = bytess[4:6]
    packed_data_type = bytess[6:8]

    assert packed_size == struct.pack("!H", gnotify.size)
    assert packed_api_type == struct.pack("!H", gnotify.api_type)
    assert packed_reserved == struct.pack("!H", gnotify.reserved)
    assert packed_data_type == struct.pack("!H", gnotify.data_type)
    assert packed_size + packed_api_type + packed_reserved + \
           packed_data_type == bytess


def test_factory_gossip_notify():
    gnotify = GossipNotify(MsgFixedSize.GOSSIP_NOTIFY, 0, 0)
    bytess = gnotify.pack()

    gnotify = APIProtocol.unpack_factory(bytess)
    asserts_gossip_notify(gnotify)


def asserts_gossip_notification(gnotification):
    assert gnotification.size >= MsgMinSize.GOSSIP_NOTIFICATION
    assert gnotification.size < MAX_MSG_SIZE
    assert gnotification.api_type == MsgType.GOSSIP_NOTIFICATION


def test_init_gossip_notification():
    gnotification = GossipNotification(MsgMinSize.GOSSIP_NOTIFICATION + 1, 0, 0, b'\x00')
    asserts_gossip_notification(gnotification)


def test_gossip_notification_strings():
    gnotification = GossipNotification(MsgMinSize.GOSSIP_NOTIFICATION + 1, 0, 0, b'\x00')
    gnotificationr = repr(gnotification)
    gnotifications = str(gnotification)
    assert MsgType.GOSSIP_NOTIFICATION.name in gnotificationr
    assert MsgType.GOSSIP_NOTIFICATION.name in gnotifications
    assert str(MsgMinSize.GOSSIP_NOTIFICATION.value + 1) in gnotificationr
    assert str(MsgMinSize.GOSSIP_NOTIFICATION.value + 1) in gnotifications


def test_pack_gossip_notification():
    gnotification = GossipNotification(MsgMinSize.GOSSIP_NOTIFICATION + 1, 0, 0, b'\x00')
    bytess = gnotification.pack()

    packed_size = bytess[:2]
    packed_api_type = bytess[2:4]
    packed_message_id = bytess[4:6]
    packed_data_type = bytess[6:8]
    packed_data = bytess[8:]

    assert packed_size == struct.pack("!H", gnotification.size)
    assert packed_api_type == struct.pack("!H", gnotification.api_type)
    assert packed_message_id == struct.pack("!H", gnotification.message_id)
    assert packed_data_type == struct.pack("!H", gnotification.data_type)
    assert packed_size + packed_api_type + packed_message_id + \
           packed_data_type + packed_data == bytess


def test_factory_gossip_notification():
    gnotification = GossipNotification(MsgMinSize.GOSSIP_NOTIFICATION + 1, 0, 0, b'\x00')
    bytess = gnotification.pack()

    gnotification = APIProtocol.unpack_factory(bytess)
    gnotification.unpack(bytess, 4)
    asserts_gossip_notification(gnotification)


def asserts_gossip_validation(gvalidation):
    assert gvalidation.size == MsgFixedSize.GOSSIP_VALIDATION
    assert gvalidation.size >= MsgMinSize.GOSSIP_VALIDATION
    assert gvalidation.size < MAX_MSG_SIZE
    assert gvalidation.api_type == MsgType.GOSSIP_VALIDATION


def test_init_gossip_validation():
    flag = GossipValidationFlags(0)
    gvalidation = GossipValidation(MsgFixedSize.GOSSIP_VALIDATION, 0, flag)
    asserts_gossip_validation(gvalidation)


def test_gossip_validation_strings():
    flag = GossipValidationFlags(0)
    gvalidation = GossipValidation(MsgFixedSize.GOSSIP_VALIDATION, 0, flag)
    gvalidationr = repr(gvalidation)
    gvalidations = str(gvalidation)
    assert MsgType.GOSSIP_VALIDATION.name in gvalidationr
    assert MsgType.GOSSIP_VALIDATION.name in gvalidations
    assert str(MsgFixedSize.GOSSIP_VALIDATION.value) in gvalidationr
    assert str(MsgFixedSize.GOSSIP_VALIDATION.value) in gvalidations


def test_pack_gossip_validation():
    flag = GossipValidationFlags(0)
    gvalidation = GossipValidation(MsgFixedSize.GOSSIP_VALIDATION, 0, flag)
    bytess = gvalidation.pack()

    assert len(bytess) == gvalidation.size

    packed_size = bytess[:2]
    packed_api_type = bytess[2:4]
    packed_message_id = bytess[4:6]
    packed_flags = bytess[6:8]

    assert packed_size == struct.pack("!H", gvalidation.size)
    assert packed_api_type == struct.pack("!H", gvalidation.api_type)
    assert packed_message_id == struct.pack("!H", gvalidation.message_id)
    assert packed_flags == struct.pack("!H", gvalidation.flags)
    assert packed_size + packed_api_type + packed_message_id + \
           packed_flags == bytess


def test_factory_gossip_validation():
    flag = GossipValidationFlags(0)
    gvalidation = GossipValidation(MsgFixedSize.GOSSIP_VALIDATION, 0, flag)
    bytess = gvalidation.pack()

    gvalidation = APIProtocol.unpack_factory(bytess)
    asserts_gossip_validation(gvalidation)


def test_undefined_api_protocol_not_implemented():
    for i in range(1500):
        # The maximum is 0xffff, but we check less since it is unlikely that
        # we ever need so much
        try:
            MsgType(i)
        except ValueError:
            with pytest.raises(NotImplementedError,
                               message=("Message type " + str(i) +
                                            " DID NOT RAISE "
                                            "<class 'NotImplementedError'>")):
                bytess = struct.pack(APIProtocol.size_type_struct.format, MIN_MSG_SIZE, i)

                APIProtocol.unpack_factory(bytess)


def test_set_too_small_size():
    with pytest.raises(ValueError):
        RPSQuery(MIN_MSG_SIZE - 1)


def brahms_factory():
    loop = asyncio.get_event_loop()
    initial_peers = [PeerProtocol(ip=IPv4Address("127.0.0.1"), onion_port=Port(3333),
                                  rps_port=Port(4444), hostkey=DERCert(os.urandom(42))),
                     PeerProtocol(ip=IPv6Address("::1"), onion_port=Port(5555),
                                  rps_port=Port(6666), hostkey=DERCert(os.urandom(4)))]
    itself = PeerProtocol(hostkey=DERCert(os.urandom(42)),
                          ip=IPv4Address("127.0.0.1"),
                          rps_port=2222,
                          onion_port=3333)
    br = Brahms(loop=loop, pushed_proportion=0.45, pulled_proportion=0.45,
                history_proportion=0.1,
                network_size=273375, ping_timeout=10, heartbeat_round_time=60,
                initial_peers=initial_peers,
                pushed_server=list(),
                brahms_round_time=5,
                itself=itself,
                nse_ip=None, nse_port=0)
    return br


def test_rps_server_query():
    try:
        global rps_config
        itself = PeerProtocol(hostkey=DERCert(os.urandom(42)),
                              ip=IPv4Address("127.0.0.1"),
                              rps_port=2222,
                              onion_port=3333)
        loop = asyncio.get_event_loop()
        apis = APIServer(loop, ["127.0.0.1"], 5555, brahms=brahms_factory(), itself=itself)

        rpsq = RPSQuery()
        rpsq_pack = rpsq.pack()

        r = asyncio.StreamReader()
        r.feed_data(rpsq_pack)
        r.feed_data(bytes(0))

        peer_buf = io.BytesIO()
        w = io.BufferedWriter(peer_buf)
        w.get_extra_info = lambda self: ("TEST-IP", 0)

        async def drain():
            pass

        w.drain = drain

        t_conn = loop.create_task(apis.handle_connection(r, w))
        t_conn2 = loop.create_task(apis.handle_rps_query(r, w, rpsq))

        while not peer_buf.getvalue():
            loop.run_until_complete(asyncio.sleep(1))
            w.flush()

        # should theoretically exit the endless loop, but does not work
        r.feed_data(bytes(0))

        rpsp_pack = peer_buf.getvalue()
        rpsp = APIProtocol.unpack_factory(rpsp_pack)
        rpsp.unpack(rpsp_pack, offset=4)

        t_conn2.cancel()
        t_conn.cancel()

    finally:
        w.close()
        peer_buf.close()

        apis.close()
        apis.wait_closed()
        loop.close()
