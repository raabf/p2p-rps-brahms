import pytest

import p2psec.rps as rps


# examples:

def test_zero_division():
        with pytest.raises(ZeroDivisionError):
                    1 / 0

def test_function():
        assert int(4) == 4
