import os
import random
from ipaddress import IPv6Address

from p2psec.rps.peer import Peer


def test_p2p_node():
    op = random.randint(1, 2 ** 16 - 1)
    rp = random.randint(1, 2 ** 16 - 1)
    ip = IPv6Address(os.urandom(16))
    hk = os.urandom(42)

    node = Peer(hostkey=hk, ip=ip, onion_port=op, rps_port=rp)

    string = str(node)

    assert str(op) in string
    assert str(rp) in string
    assert str(ip) in string

    repres = repr(node)

    assert str(op) in repres
    assert str(rp) in repres
    assert str(ip) in repres
    assert hk.hex() in repres
