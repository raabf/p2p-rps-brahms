import os
import random

import pytest

from p2psec.rps.p2p.p2p_protocol import P2PProtocol, Pong, RPSMsgFixedSize


def test_pong_size():
    req = Pong()

    assert req.size == RPSMsgFixedSize.PONG
    assert req.pong_struct.size == RPSMsgFixedSize.PONG

    rn = random.uniform(5, 50)

    req = Pong(size=rn)
    assert req.size == rn


def test_pong_nonce():
    req = Pong()
    assert req.nonce is None

    nonce = os.urandom(8)
    req = Pong(nonce=nonce)
    assert req.nonce == nonce


def test_pong_pack_unpack():
    nonce = os.urandom(8)
    req = Pong(nonce=nonce)
    packed = req.pack()

    req2 = Pong()
    req2.unpack(packed, offset=0)

    assert req.size == req2.size
    assert req.nonce == req2.nonce


def test_pong_unpack():
    nonce = os.urandom(8)
    packed = (RPSMsgFixedSize.PONG.to_bytes(2, "big", signed=False) +
              bytes(2) +
              nonce)

    req = Pong()
    req.unpack(packed, offset=0)

    assert req.size == RPSMsgFixedSize.PONG
    assert req.nonce == nonce


def test_pong_pack():
    nonce = os.urandom(8)
    packed = (RPSMsgFixedSize.PONG.to_bytes(2, "big", signed=False) +
              bytes(2) +
              nonce)

    req = Pong(size=RPSMsgFixedSize.PONG, nonce=nonce)

    assert packed == req.pack()


def test_pong_prints():
    nonce = os.urandom(8)
    req = Pong(nonce=nonce)

    _repr = repr(req)
    _str = str(req)

    assert str(int(RPSMsgFixedSize.PONG)) in _repr
    assert str(int(RPSMsgFixedSize.PONG)) in _str

    assert nonce.hex() in _repr
    assert nonce.hex() in _str
