from ipaddress import IPv4Address, IPv6Address
from os import urandom
from random import uniform

import pytest

from p2psec.rps.globals import Automatic
from p2psec.rps.p2p.p2p_pull import PullReply
from p2psec.rps.p2p.peer_protocol import PeerProtocol


def random_peer_proto():
    oport = int(uniform(1, 2 ** 16 - 1))
    rport = int(uniform(1, 2 ** 16 - 1))
    der = urandom(42)

    if uniform(0, 1) < 0.5:
        ip = IPv4Address(urandom(4))
    else:
        ip = IPv6Address(urandom(16))

    peerp = PeerProtocol(hostkey=der, ip=ip, onion_port=oport,
                         rps_port=rport, size=Automatic, ipv=Automatic)

    return peerp


def test_p2p_pull():
    lst = list(random_peer_proto() for _ in range(int(uniform(5, 42))))
    size_peers = 0
    for peer in lst:
        size_peers += peer.size
    nonce = urandom(8)

    pr = PullReply(nonce=nonce, peers=lst)

    assert pr.nonce == nonce
    assert pr.size == size_peers + 12

    pp = PeerProtocol(ipv=4)
    pr.peers.append(pp)

    with pytest.warns(UserWarning):
        assert pr.size is None


def test_p2p_pull_pack_single():
    pp = random_peer_proto()
    size = pp.size + 12
    nonce = urandom(8)
    pull_header = (size.to_bytes(2, "big", signed=False) + bytes(2) + nonce)

    packed = pull_header + pp.pack()

    pr = PullReply(nonce=nonce, peers=[pp, ])
    pr_packed = pr.pack()

    assert pr_packed == packed


def test_p2p_pull_unpack_single():
    pp = random_peer_proto()
    size = pp.size + 12
    nonce = urandom(8)
    pull_header = (size.to_bytes(2, "big", signed=False) + bytes(2) + nonce)

    packed = pull_header + pp.pack()

    pr = PullReply()
    pr.unpack(packed, offset=0)

    assert pr.size == size
    assert pr.nonce == nonce
    assert pr.peers[0].hostkey == pp.hostkey
    assert pr.peers[0].ip == pp.ip


def test_p2p_pull_pack_multiple():
    lst = list(random_peer_proto() for _ in range(int(uniform(5, 42))))
    size = 12
    for peer in lst:
        size += peer.size

    nonce = urandom(8)
    pull_header = (size.to_bytes(2, "big", signed=False) + bytes(2) + nonce)
    packed = pull_header

    for peer in lst:
        packed += peer.pack()

    pr = PullReply(nonce=nonce, peers=lst)
    pr_packed = pr.pack()

    assert pr_packed == packed


def test_p2p_pull_unpack_multiple():
    lst = list(random_peer_proto() for _ in range(int(uniform(5, 42))))
    size = 12
    for peer in lst:
        size += peer.size

    nonce = urandom(8)
    pull_header = (size.to_bytes(2, "big", signed=False) + bytes(2) + nonce)
    packed = pull_header

    for peer in lst:
        packed += peer.pack()

    pr = PullReply()
    pr.unpack(packed)

    assert pr.size == size
    assert pr.nonce == nonce

    assert len(lst) == len(pr.peers)

    for p1, p2 in zip(lst, pr.peers):
        assert p1.ip == p2.ip
        assert p1.hostkey == p2.hostkey


def test_p2p_pull_prints():
    lst = list(random_peer_proto() for _ in range(int(uniform(2, 7))))
    nonce = urandom(8)

    pr = PullReply(nonce=nonce, peers=lst)

    repr_ = repr(pr)

    assert repr(pr.size) in repr_
    assert nonce.hex() in repr_

    str_ = str(pr)

    assert repr(pr.size) in str_
    assert nonce.hex() in str_
