import asyncio
import os
import random
from ipaddress import IPv4Address, IPv6Address

from p2psec.rps.brahms import Brahms
from p2psec.rps.globals import DERCert, Port
from p2psec.rps.p2p.peer_protocol import PeerProtocol
from p2psec.rps.peer import Peer


def brahms_factory():
    loop = asyncio.get_event_loop()
    initial_peers = [PeerProtocol(ip=IPv4Address("127.0.0.1"), onion_port=Port(3333),
                          rps_port=Port(4444), hostkey=DERCert(os.urandom(42))),
                     PeerProtocol(ip=IPv6Address("::1"), onion_port=Port(5555),
                          rps_port=Port(6666), hostkey=DERCert(os.urandom(4)))]
    itself = PeerProtocol(hostkey=DERCert(os.urandom(42)),
                          ip=IPv4Address("127.0.0.1"),
                          rps_port=2222,
                          onion_port=3333)
    br = Brahms(loop=loop, pushed_proportion=0.45, pulled_proportion=0.45,
                history_proportion=0.1,
                network_size=273375, ping_timeout=10, heartbeat_round_time=60,
                initial_peers=initial_peers,
                pushed_server=list(),
                brahms_round_time=5,
                itself=itself,
                nse_ip=None, nse_port=0)
    return br


def test_rand_choices():
    br = brahms_factory()
    length = int(random.uniform(1, 42))
    sub_len = int(random.uniform(1, length))

    lst = [object() for _ in range(length)]

    slst = br._rand_choices(lst, sub_len)

    assert len(slst) <= len(lst)

    for o in slst:
        assert o in lst

    slst = br._rand_choices([], 0)
    assert not slst


def test_rand_peer():
    br = brahms_factory()
    # TODO test_rand_peer()


def test_local_view_size():
    br = brahms_factory()

    pusheds, pulleds, hists = br._local_view_sizes_from_minimum(45, 48)

    assert pusheds == 45
    assert pulleds == 45
    assert hists == 10

    pusheds, pulleds, hists = br._local_view_sizes_from_estimation(45 ** 3 * 3)

    assert pusheds == 45
    assert pulleds == 45
    assert hists == 10
