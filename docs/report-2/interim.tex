% !TeX spellcheck = en_GB
\documentclass[a4paper]{article}
\usepackage{xcolor}
\usepackage{colorspace}
\usepackage{amsmath}
\usepackage{fancyhdr, graphicx}
\usepackage{enumitem}
\usepackage{url}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{todonotes}
\usepackage{listings}
\usepackage[margin=3cm]{geometry}
\usepackage[backend=bibtex, style=numeric]{biblatex}
\bibliography{library} 

%%% TUM Colours %%%

% Primary
\definecolor{tum-blue}{RGB}{0,101,189}
\definecolor{tum-white}{RGB}{255,255,255}
\definecolor{tum-black}{RGB}{0,0,0}

% Secondary

\definecolor{tum-blue-darker}{RGB}{0,82,147}
\definecolor{tum-blue-darkest}{RGB}{0,51,89}

% three gray colours 80%, 50%, and 20%
\definecolor{tum-gray80}{RGB}{088,088,090}
\definecolor{tum-gray50}{RGB}{156,157,159}
\definecolor{tum-gray20}{RGB}{217,218,219}

% accent

\definecolor{tum-accent-gray}{RGB}{218,215,203}
\definecolor{tum-accent-orange}{RGB}{227,114,34}
\definecolor{tum-accent-green}{RGB}{162,173,0}
\definecolor{tum-accent-blue-light}{RGB}{152,198,234}
\definecolor{tum-accent-blue-dark}{RGB}{100,160,200}

\renewcommand{\headrulewidth}{0pt}


\fancyhead[L]{\color{tum-blue}
  Chair for Network Architectures and Services\\
  Department of Informatics\\
  Technical University of Munich
}
\fancyhead[R]{
  \includegraphics[height=1.3cm]{tum-logo.pdf}
}

\title{Peer to Peer Systems and Security: Team 21\\
  \large Random Peer Sampling: Interim Report}
\date{Summer Semester 2017 \textbullet\space 20. June 2017}
\author{
  Fabian Raab\\
  \href{mailto:Fabian Raab <fabian.raab@tum.de>}{fabian.raab@tum.de}
  \and
  Joao Neto\\
  \href{mailto:Joao Neto <neto@in.tum.de>}{neto@in.tum.de}
}

%% listings %%

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
	backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
	basicstyle=\footnotesize\ttfamily,% the size of the fonts that are used for the code
	breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 % sets automatic line breaking
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{tum-gray80},    % comment style
	deletekeywords={...},            % if you want to delete keywords from the given language
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
	frame=single,                    % adds a frame around the code
	identifierstyle=\color{tum-accent-green}, % identifier style
	keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	keywordstyle=\color{tum-blue-darker},	 % keyword style
	language=C,                 	 % the language of the code
	morekeywords={*,...},            % if you want to add more keywords to the set
	numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
	numbersep=5pt,                   % how far the line-numbers are from the code
	numberstyle=\tiny\color{tum-gray50}, % the style that is used for the line-numbers
	rulecolor=\color{tum-black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=true ,          % underline spaces within strings only
	showtabs=false,                  % show tabs within strings adding particular underscores
	stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
	stringstyle=\color{tum-accent-orange},     % string literal style
	tabsize=2,                       % sets default tabsize to 2 spaces
	title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\begin{document}



\maketitle
\thispagestyle{fancy}

\begin{flushleft}
  % bla
\end{flushleft}

\bigskip

In order to avoid locks, mutexes and a tough debugging process with multi-threaded programs, which are more complicated and typically more error prone, we ended up choosing a single-threaded program for the RPS module implementation. 
Although, we want to be able to deal with several requests, so we decided to use event loops, which is a non-blocking I/O approach.
The usual way to use an event loop, at least from our previous experience, is with callbacks.
With callbacks, the application registers callback functions, which are invoked from the main loop when particular events occur. However, after some additional research, it was consensual that using the \texttt{asyncio} core library with the coroutine approach is more beneficial. This library calls coroutines implemented by us. With these coroutines, we can use the \texttt{await} instruction during every coroutine function, which enables us to switch between tasks and run other instructions, instead of blocking in a certain sub-task that requires a waiting result.
Additionally, coroutines also provide a streams API, which is just a set of wrappers around the callbacks that makes simpler the sending and receiving of messages.
So, in the end we take advantage of a very good asynchronous framework, with no CPU context switch, no race conditions and consequently dead-lock free.

\bigskip
For the design of the module's P2P protocol, we used one of the suggestions given in the project specification, this is, Cap'n Proto \cite{capnproto}, since it is an abstraction layer library which is simple, really fast and strongly-typed.
\bigskip

\begin{lstlisting}[caption={Request message}, language=c, label=request]
enum RequestType @0xdd2662c60bb48efb {
	ping @0;
	pull @1;
	push @2;
}

struct Request @0xdd564591a131d08f {
	nonce @0 :UInt32;
	type @1 :RequestType;
}
\end{lstlisting}

As a Random Peer Sampling algorithm, \emph{Brahms}\cite{bortnikov_brahms:_2008} has been chosen by us, since it is a stable, well developed, and secure algorithm. Brahms requires to exchange messages for three purposes namely still-alive, push, and pull. For all three possibilities a \verb|Request| has to be sent to another peer as defined in figure~\ref{request}.
All requests also contain a nonce which have to be included in the response to make sure we receive the answer from the connected peer and not from an attacker or another module. In this way we can authenticate our module running on different peers. The responses of those three protocol-runs differ and therefore will be described in the following.

\begin{lstlisting}[caption={Pong message}, language=c, label=pong]
struct Pong @0xf920e44128afad88 {
	nonce @0 :UInt32;
}
\end{lstlisting}

First, peers have to be queried to check if they are still alive. For that we have a simple ping protocol-run which contains a \verb|Request| of type \verb|ping| and a reply \verb|Pong| (figure~\ref{pong}):
\begin{center}
\verb|Request(type = ping)| $\rightarrow$ \verb|Pong|
\end{center}

\begin{lstlisting}[caption={NodeInfo message}, language=c, label=node-info]
struct NodeInfo @0xe90c19880ae37ed3 {
	ip @0 :Data;
	rpsPort @1 :UInt32;
	onionPort @2 :UInt32;
	hostkey @3 :Data;
}
\end{lstlisting}

\begin{lstlisting}[caption={PullReply message}, language=c, label=pull-reply]
struct PullReply @0xc4232b1524b9912b {
	nonce @0 :UInt32;
	nodes @1 :List(NodeInfo);
}
\end{lstlisting}

Furthermore, Brahms requires to exchange peer information as defined in figure~\ref{node-info} to be able to build a local view. The algorithm requests peer information from random other nodes via a pull request and receives by chance peer information from other nodes, which select it at random. This is done for security reasons, because it mitigates through the combined use of push and pull the possibility to poison the local view by an attacker\cite{bortnikov_brahms:_2008}. A pull request follows a list of multiple nodes in a \verb|PullReply| (figure~\ref{pull-reply}):

\begin{center}
\verb|Request(type = pull)| $\rightarrow$ \verb|PullReply|
\end{center}

\bigskip

\begin{lstlisting}[caption={PushChallenge and PushReply message}, language=c, label=push-run]
struct PushChallenge @0xb509a9ff1a364b78 {
	nonce @0 :UInt32;
	challenge @1 :UInt32;
}

struct PushReply @0xb43dc0d003447624 {
	answer @0 :UInt32;
	nodeInfo @1 :NodeInfo;
}
\end{lstlisting}

A node can still be poisoned or DoSed with push-requests frequently sent. To prevent that, the sender of the push request have to do a \emph{proof-of-work} before its push gets accepted. In order to do this, first a \verb|PushChallenge| is sent and the \verb|PushReply| (figure~\ref{push-run}) then contains the answer to this challenge as well as the actual peer information:

\begin{center}
\verb|Request(type = push)| $\rightarrow$ \verb|PullChallenge| $\rightarrow$ \verb|PullReply|
\end{center}

\bigskip

\begin{lstlisting}[caption={Hello message}, language=c, label=hello]
@0xe848ef027c14d63a;
struct RPSHello @0x8fb64116cd5a05d7 {
	node @0 :NodeInfo;
}
\end{lstlisting}

When this RPS module starts its local view is empty and no other one knows its existence, wherefore it is impossible to send pushes or to receive pulls. To initially fill its local view it asks the gossip module to propagate the \verb|RPSHello| (figure~\ref{hello}). The gossip module can learn new peers through a bootstrapper or trough a static configuration and is therefore able to deliver messages to other ones even during initialization. Everyone who receives should send a push to initialize the local view of the new node in the network:

\begin{center}
\verb|GossipAnnunce(type = RPS_HELLO, data = RPSHello)| $\rightarrow$ \emph{Push-run}
\end{center}

The error handling is quite simple.
If a peer go down the ping-run will detect that and will remove the node from the local view. Every mentioned protocol-run gets an appropriate timeout. If the timeout fires or we received a malformed message the ping-run will fail, and all other ones gets cancelled. The Brahms algorithm even allows that all push and pulls fail, so aborting them is not an issue.

\printbibliography

\end{document}
