% !TeX spellcheck = en_GB
\documentclass[a4paper]{article}
\usepackage{xcolor}
\usepackage{colorspace}
\usepackage{amsmath}
\usepackage{fancyhdr, graphicx}
\usepackage{enumitem}
\usepackage{url}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{todonotes}
\usepackage{listings}
\usepackage[margin=3cm]{geometry}
\usepackage[backend=bibtex, style=numeric]{biblatex}
\usepackage[flushleft]{threeparttable} % http://ctan.org/pkg/threeparttable
\usepackage{booktabs,caption}
\usepackage{bytefield}
\usepackage{float}
\bibliography{library} 

%%% TUM Colours %%%

% Primary
\definecolor{tum-blue}{RGB}{0,101,189}
\definecolor{tum-white}{RGB}{255,255,255}
\definecolor{tum-black}{RGB}{0,0,0}

% Secondary

\definecolor{tum-blue-darker}{RGB}{0,82,147}
\definecolor{tum-blue-darkest}{RGB}{0,51,89}

% three gray colours 80%, 50%, and 20%
\definecolor{tum-gray80}{RGB}{088,088,090}
\definecolor{tum-gray50}{RGB}{156,157,159}
\definecolor{tum-gray20}{RGB}{217,218,219}

% accent

\definecolor{tum-accent-gray}{RGB}{218,215,203}
\definecolor{tum-accent-orange}{RGB}{227,114,34}
\definecolor{tum-accent-green}{RGB}{162,173,0}
\definecolor{tum-accent-blue-light}{RGB}{152,198,234}
\definecolor{tum-accent-blue-dark}{RGB}{100,160,200}

\renewcommand{\headrulewidth}{0pt}


\fancyhead[L]{\color{tum-blue}
  Chair for Network Architectures and Services\\
  Department of Informatics\\
  Technical University of Munich
}
\fancyhead[R]{
  \includegraphics[height=1.3cm]{tum-logo.pdf}
}

\title{Peer to Peer Systems and Security: Team 21\\
  \large Random Peer Sampling: Final Report}
\date{Summer Semester 2017 \textbullet\space 20. August 2017}
\author{
  Fabian Raab\\
  \href{mailto:Fabian Raab <fabian.raab@tum.de>}{fabian.raab@tum.de}
  \and
  Jo$\tilde{\text{a}}$o Neto\\
  \href{mailto:Joao Neto <neto@in.tum.de>}{neto@in.tum.de}
}

%% listings %%

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
	backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
	basicstyle=\footnotesize\ttfamily,% the size of the fonts that are used for the code
	breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
	breaklines=true,                 % sets automatic line breaking
	captionpos=b,                    % sets the caption-position to bottom
	commentstyle=\color{tum-gray80},    % comment style
	deletekeywords={...},            % if you want to delete keywords from the given language
	escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
	extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
	frame=single,                    % adds a frame around the code
	identifierstyle=\color{tum-accent-green}, % identifier style
	keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
	keywordstyle=\color{tum-blue-darker},	 % keyword style
	language=C,                 	 % the language of the code
	morekeywords={*,...},            % if you want to add more keywords to the set
	numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
	numbersep=5pt,                   % how far the line-numbers are from the code
	numberstyle=\tiny\color{tum-gray50}, % the style that is used for the line-numbers
	rulecolor=\color{tum-black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
	showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
	showstringspaces=true ,          % underline spaces within strings only
	showtabs=false,                  % show tabs within strings adding particular underscores
	stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
	stringstyle=\color{tum-accent-orange},     % string literal style
	tabsize=2,                       % sets default tabsize to 2 spaces
	title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\begin{document}



\maketitle
\thispagestyle{fancy}

\begin{flushleft}
  % bla
\end{flushleft}

\section{Software overview}

This project realizes a \emph{Random Peer Sampling} (RPS) algorithm called \emph{Brahms}\cite{bortnikov_brahms:_2008}. We archived to successfully implement all features of this algorithm.
Our module has to interact with two other ones, Gossip for an initial peer list, and NSE for an network estimation to decide to how many other peers request for an local view update should be sent.

At the start of the module an \verb|RPSHello| is sent to Gossip and therefore propagated through the overlay network. Every other RPS module receiving this \verb|RPSHello| has to do push with its own implementation to the requesting peer. The requesting peer waits some configurable time until some pushes are received and then starts the brahms algorithm with this initial peers list.

The Brahms algorithm works in rounds. In the beginning of each round the NSE module is asked for network size estimation. The amount of pulls and pushes sent to other peers are calculated depending on this estimation. The local view is then shrank again depending on the peers who really answered the request. However the use of the estimation is important to allow the local view to grow again if there are more peers in the network again who can answer the requests. 

Our pushes include a proof-of-work to mitigate poisoning attacks. For that an algorithm from an external library called hashcash is used.

The Sampler requires a min-wise independent permutation function for which we used a seedable hash function called xxhash.

Beside the mentioned points everything is self implemented according to \cite{bortnikov_brahms:_2008}. 

\bigskip

The program needs at least python 3.5.1 and was tested with python 3.5.2, 3.5.3, 3.6.1, and 3.7-dev. It was tested on Ubuntu 16.04 with a Linux kernel of 4.4 and higher. We do not expect any problems with other Linux derivatives and theoretically it should work on any OS which supports python.

The dependencies can be installed via \verb|pip|, please refer to \verb|README.md| and \verb|requirements.txt| for further details.

The program can be executed with two commands. \verb|run| will start it normally like described so far and \verb|noexternal| will start in without the Gossip and NSE dependencies. The initial peers list and the estimation will then be replaced by static values. Please refer to \verb|README.md| for further details. There are some testing processes our programm sucessfully passes. Again see \verb|README.md| for further details.

\bigskip

We are not sure how good the distribution of RPSHello over gossip works, so much more testing has to be done. In general more testing is required, especially with a larger network, to improve error handling. Also performance was not under consideration until yet, although sometimes we pay attention to use the more efficient way if we notice a problem. Beside that, we use cryptographically secure functions\footnote{Except xxhash, but we are not sure if the sampler requires a cryptographic secure hash function} and we fully implement the brahms algorithm, which leads to a good and an already against attacks resilient RPS algorithm.

\section{Project division \& effort}

\subsection*{Fabian}

\begin{center}

%\begin{table*}
%	\centering
%	\caption{Tabular Material}
	\begin{threeparttable}
		\begin{tabular}{llll}
			\toprule
			Testing environment\tnote{a}                                                                & 5h  &  \\
			command line parsing \verb|rps.__main__|                                                    & 6h  &  \\
			hierarchy design \verb|rps.api.APIProtocol|/\verb|rps.p2p.P2PProtocol|                      & 10h &  \\
			\verb|rps.api.api_protocol.RPSQuery|/\verb|RPSPeer|                                         & 3h  &  \\
			\verb|rps.api.APIServer| class, \verb|handle_connection|, \verb|handle_rps_query| \tnote{b} & 14h &  \\
			\verb|rps.p2p.client.ping|/\verb|pull|                                                      & 6h  &  \\
			\verb|rps.p2p.p2p_protocol.Request|                                                         & 4h  &  \\
			\verb|rps.p2p.p2p_protocol.Pong|                                                            & 3h  &  \\
			\verb|rps.p2p.p2p_pull|                                                                     & 2h  &  \\
			\verb|rps.p2p.peer_protocol|                                                                & 5h  &  \\
			\verb|rps.p2p.P2PServer| except \verb|handle_push|                                          & 4h  &  \\
			heartbeat and Brahms algorithm: \verb|rps.brahms|\tnote{c}                                  & 15h &  \\
			sampler: \verb|rps.sampler|                                                                 & 5h  &  \\
			understand Cap'n Proto and how to integrate it to python's \verb|asyncio|                   & 8h  &  \\ \bottomrule
		\end{tabular}
		\begin{tablenotes}
			\item[a] including, CI system, tox (unit testing in a clean virtual environment with different python versions), pytest (unit testing), and mypy (static type checking)
			\item[b] Here was so difficult to decide the general architecture: streams and callbacks. We compared the sever architectures and decided for the streams. Additionally it was a huge problem for me to correctly closing the loop and server without crashing the server or having a complaining loop that tasks are not finished. Well the solution is the \verb|except CancelledError:| in \verb|server.py| and the code in \verb|finally:| in \verb|__main__.py|.
			\item[c] The main challenge here was to make python \verb|asyncio| functions correctly working, including correct error handling. Then the algorithms itself were quite fast implemented.
		\end{tablenotes}
	\end{threeparttable}
%\end{table*}
\end{center}

The time column is very rough estimation of the required hours, which I was able to refactor. This column also includes the time required to write the unit tests for the described source code unit. There are additional 10h for understanding Brahms and probably more to bug fixing as well as understating libraries (it is hard to remember so specific). Overall just the protocol parsing of the API and P2P took over 70\% of the time, 25\% for the client/server and minor 5\% for the RPS algorithm itself. On average I worked 3 days per week for about 3h per day except the last two weeks before the subm


\subsection*{Joao}

\begin{center}

%\begin{table*}
%	\centering
%	\caption{Tabular Material}
	\begin{threeparttable}
		\begin{tabular}{llll}
			\toprule
            \verb|rps.api.api_protocol.NSEQuery|/\verb|NSEEstimate|                                     & 6h  &  \\
			\verb|rps.api.api_protocol.GossipAnnounce|/\verb|GossipNotify|								& 6h  &  \\
            \verb|rps.api.api_protocol.GossipNotification|/\verb|GossipValidation|        				& 7h  &  \\
            \verb|rps.api.client.nse_query|/\verb|gossip_announce|/\verb|gossip_notify|					& 5h  &  \\
			\verb|rps.api.client.notify_rps_hello|/\verb|rps_hello|	              					    & 1h  &  \\
			\verb|rps.api.server.handle_gossip_notification|							                & 3h  &  \\
			\verb|rps.p2p.client.push|\tnote{a}                                                         & 6h  &  \\
			\verb|rps.p2p.server.handle_push|\tnote{a}                                        			& 5h  &  \\ 
            \verb|rps.p2p.p2p_push|                                        								& 8h  &  \\ \bottomrule
		\end{tabular}
        \begin{tablenotes}
			\item[a] These included the search for a proof-of-work system already implemented and compatible in Python.
            We ended up choosing an implementation of Hashcash.
		\end{tablenotes}
	\end{threeparttable}
%\end{table*}
\end{center}

Like Fabian's, my time column is a rough estimation of the required hours. This column also include the time required to write the unit tests for the described source code unit. There are additional 4h or 5h for understanding certain details that were not very clear to me about Brahms. There are additional 15h approximately that I used to implement a P2P client and server using Cap'n Proto, although we then decided to change the protocol and it remains on a branch that we didn't merge to master. On average I worked 2 days per week for about 3h to 4h per day and the workload increased in the last 2 weeks.

\section{Protocol documentation}

\bigskip
In the previous report (Interim report), for the design of the module's P2P protocol, we used one of the suggestions given in the project specification, this is, Cap'n Proto \cite{capnproto}, since it is an abstraction layer library which is simple, really fast and strongly-typed. Although, since its implementation in Python don't use the \verb|asyncio| library, which is necessary for our project, it was not a feasible option for the protocol. Given this said, we ended up creating our own protocol, using the API protocol as a base and adapting it to our needs.
\bigskip 

\subsection*{Request}


As a Random Peer Sampling algorithm, \emph{Brahms}\cite{bortnikov_brahms:_2008} has been chosen by us, since it is a stable, well developed, and secure algorithm. Brahms requires to exchange messages for three purposes namely still-alive, push, and pull. For all three possibilities a \verb|Request| (figure \ref{fig:request}) has to be sent to another peer.
In this P2P protocol implementation all the messages have to have a \verb|size| field, so that the receiver knows how many bytes should be read.
All requests also contain a nonce which have to be included in the response to make sure we receive the answer from the connected peer and not from an attacker or another module. In this way, we can detect our module running on different peers. The responses of those three protocol-runs differ and therefore will be described in the following subsections. The \verb|reserved| field works like a padding, in order to be simple to parse the packets. \\

\begin{figure}[H]
\centering
    \begin{bytefield}[bitwidth=1.0em]{32}
        \bitheader{0,7,8,15,16,23,24,31} \\
        \bitbox{16}{size} & \bitbox{8}{reserved} & 
        \bitbox{8}{request type} \\
        \wordbox{2}{nonce (64 bits)} \\
    \end{bytefield}
\caption{Request message format}
\label{fig:request}
\end{figure}

A peer client sends a \verb|Request| with one of the \verb|request type|s 3 available:
\begin{itemize}
\item 1. Ping 
\item 2. Push
\item 3. Pull
\end{itemize}

\subsection*{Pong}

Peers have to be queried to check if they are still alive. For that we have a simple ping protocol-run which contains a \verb|Request| of type \verb|Ping| and a reply \verb|Pong|):
\begin{center}
\verb|Request(request type = Ping)| $\rightarrow$ \verb|Pong|
\end{center}    

\begin{figure}[H]
\centering
    \begin{bytefield}[bitwidth=1.0em]{32}
        \bitheader{0,7,8,15,16,23,24,31} \\
        \bitbox{16}{size} & 
        \bitbox{16}{reserved} \\
        \wordbox{2}{nonce (64 bits)} \\
    \end{bytefield}
\caption{Pong message format}
\label{fig:pong}
\end{figure}  

\bigskip

\subsection*{PullReply}


\begin{figure}[H]
\centering
    \begin{bytefield}[bitwidth=1.0em]{32}
        \bitheader{0,7,8,15,16,23,24,31} \\
        \bitbox{16}{size} & 
        \bitbox{16}{reserved} \\
        \wordbox{2}{nonce (64 bits)} \\
        \begin{rightwordgroup}{Peer 1}
          \bitbox{16}{size} & \bitbox{8}{IP version} & 
          \bitbox{8}{reserved} \\
          \bitbox{16}{rps port} & \bitbox{16}{onion port} \\
          \wordbox{2}{IPv4 address (32 bits)/IPv6 address (128 bits)} \\
          \wordbox[lrt]{1}{peer's hostkey in DER format} \\
          \skippedwords \\
          \wordbox[lrb]{1}{}
        \end{rightwordgroup} \\
        \wordbox[]{1}{$\vdots$} \\[1ex]
        \begin{rightwordgroup}{Peer N}
          \bitbox{16}{size} & \bitbox{8}{IP version} & 
          \bitbox{8}{reserved} \\
          \bitbox{16}{rps port} & \bitbox{16}{onion port} \\
          \wordbox{2}{IPv4 address (32 bits)/IPv6 address (128 bits)} \\
          \wordbox[lrt]{1}{peer's hostkey in DER format} \\
          \skippedwords \\
          \wordbox[lrb]{1}{}
        \end{rightwordgroup} \\
    \end{bytefield}
\caption{PullReply message format}
\label{fig:pullreply}
\end{figure}  
\bigskip

Furthermore, Brahms requires to exchange peer information to be able to build a local view. The algorithm requests peer information from random other nodes via a pull request and receives (by chance) peer information from other nodes, which select it at random. This is done for security reasons, because it mitigates through the combined use of push and pull the possibility to be poisoned in the local view by an attacker\cite{bortnikov_brahms:_2008}. A pull request follows a list of multiple nodes in a \verb|PullReply| (figure \ref{fig:pullreply}):

\begin{center}
\verb|Request(request type = Pull)| $\rightarrow$ \verb|PullReply|
\end{center}

The peer information, is represented in figure \ref{fig:pullreply} as the \verb|Peer| block. A \verb|PullReply| has a list of these blocks, since the sender forwards all the peers in its local view. Each block contains a \verb|size| which is used to calculate the size of the block before calculating the whole message size from the sender side and is required to unpack the peer's blocks inside the message in the receiver side. It also contains a \verb|IP version| which indicates if the IP address of the peer is IPv4 or IPv6, a \verb|rps port| that is available to establish the P2P communication, a \verb|onion port| that is used for the API communication and finally the peer's \verb|IP address| and \verb|hostkey|.

\subsection*{PushChallenge}


A node can still be poisoned or DoSed with push-requests frequently sent. To prevent that, the sender of the push request have to do a \emph{proof-of-work} before its push gets accepted. In order to do this, first a \verb|PushChallenge| (figure \ref{fig:pushchallenge}) is sent and the \verb|PushReply| (figure \ref{fig:pushreply}) then it contains the answer to this challenge as well as the actual peer information:

\begin{center}
\verb|Request(request type = Push)| $\rightarrow$ \verb|PushChallenge| $\rightarrow$ \verb|PushReply|
\end{center}

\begin{figure}[H]
\centering
    \begin{bytefield}[bitwidth=1.0em]{32}
        \bitheader{0,7,8,15,16,23,24,31} \\
        \bitbox{16}{size} & 
        \bitbox{16}{reserved} \\
        \wordbox{2}{nonce (64 bits)} \\
        \wordbox{2}{resource (64 bits)} \\
        \bitbox{8}{difficulty} \\
    \end{bytefield}
\caption{PushChallenge message format}
\label{fig:pushchallenge}
\end{figure}
\bigskip

In \verb|PushChallenge|, there is the normal P2P protocol header, as well as a 64 bits \verb|resource| which is a random string that the client should use to hash it and perform the \emph{proof-of-work} with Hashcash. There is also a 8 bit \verb|difficulty| that is just a simple integer that Hashcash uses as a parameter to hash the \verb|resource|. The higher this integer is, the more difficult is the challenge. By default, it is 20, meaning that the first 20 bits of the hash are all zeros. So, if the sender is computing a 160-bit SHA-1 hash, she needs to try on average $2^{20}$ counter values, which will take about 1 second to find and it is a reasonable time.

\subsection*{PushReply}

\begin{figure}[H]
\centering
    \begin{bytefield}[bitwidth=1.0em]{32}
        \bitheader{0,7,8,15,16,23,24,31} \\
        \bitbox{16}{size} & 
        \bitbox{16}{reserved} \\
        \wordbox{2}{nonce (64 bits)} \\
        \begin{rightwordgroup}{Peer}
          \bitbox{16}{size} & \bitbox{8}{IP version} & 
          \bitbox{8}{reserved} \\
          \bitbox{16}{rps port} & \bitbox{16}{onion port} \\
          \wordbox{2}{IPv4 address (32 bits)/IPv6 address (128 bits)} \\
          \wordbox[lrt]{1}{peer's hostkey in DER format} \\
          \skippedwords \\
          \wordbox[lrb]{1}{}
        \end{rightwordgroup} \\
        \wordbox[lrt]{1}{answer} \\
        \skippedwords \\
        \wordbox[lrb]{1}{}
    \end{bytefield}
\caption{PushReply message format}
\label{fig:pushreply}
\end{figure}
\bigskip


\verb|PushReply| contains a P2P protocol header, a \verb|Peer| block, containing the sender information and finally the \verb|answer| of the received challenge that need to be checked afterwards by the P2P server.

\subsection*{RPSHello}

When this RPS module starts, its local view is empty and no other one knows its existence, wherefore it is impossible to send pushes or to receive pulls. To initially fill its local view it asks the gossip module to propagate the \verb|RPSHello| (figure \ref{fig:rpshello}). The gossip module can learn new peers through a bootstrapper or through a static configuration and is therefore able to deliver messages to other peers even during initialization. Everyone who received should send a \verb|Push| to initialize the local view of the new node in the network:

\begin{center}
\verb|GossipAnnounce(data type = RPS HELLO, data = RPSHello)| $\rightarrow$ \emph{Push-run}
\end{center}

\begin{figure}[H]
\centering
    \begin{bytefield}[bitwidth=1.0em]{32}
        \bitheader{0,7,8,15,16,23,24,31} \\
        \bitbox{16}{size} & 
        \bitbox{16}{GOSSIP ANNOUNCE} \\
        \bitbox{8}{TTL}  & \bitbox{8}{reserved} 
        & \bitbox{16}{RPS HELLO} \\
        \begin{rightwordgroup}{Peer}
          \bitbox{16}{size} & \bitbox{8}{IP version} & 
          \bitbox{8}{reserved} \\
          \bitbox{16}{rps port} & \bitbox{16}{onion port} \\
          \wordbox{2}{IPv4 address (32 bits)/IPv6 address (128 bits)} \\
          \wordbox[lrt]{1}{peer's hostkey in DER format} \\
          \skippedwords \\
          \wordbox[lrb]{1}{}
        \end{rightwordgroup} \\
    \end{bytefield}
\caption{RPSHello message format}
\label{fig:rpshello}
\end{figure}

The RPSHello message is just a GossipAnnounce filled with the \verb|RPS HELLO| code in the \verb|data type| and a \verb|Peer| information in the \verb|data| field.

\section{Future work}

As already mentioned, if we would have more time, we wanted to much more testing cases, mainly some functional and security tests.
We also aim to improve the error handling cases where it is needed, as well as improve the interaction with other modules. Of course, these last ones are a direct consequence of a better testing. Finally, we would fix all the known issues that we cover in the \verb|Software overview|.

\printbibliography

\end{document}
