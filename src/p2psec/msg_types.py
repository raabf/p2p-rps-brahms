import enum

import aenum

MIN_MSG_SIZE = 4
MAX_MSG_SIZE = 64 * 1024  # 64 KiB


class MsgType(enum.IntEnum):
    """Class used as enumeration to distinguish
        between messages received in the APIProtocol.
    """
    GOSSIP_ANNOUNCE = 500
    GOSSIP_NOTIFY = 501
    GOSSIP_NOTIFICATION = 503
    GOSSIP_VALIDATION = 504
    # reserved for gossip until 519

    NSE_QUERY = 520
    NSE_ESTIMATE = 521
    # reserved for NSE until 539

    RPS_QUERY = 540
    RPS_PEER = 541
    RPS_HELLO = 52195
    # reserved for RPS until 559

    ONION_TUNNEL_BUILD = 560
    ONION_TUNNEL_READY = 561
    ONION_TUNNEL_INCOMING = 562
    ONION_TUNNEL_DESTROY = 563
    ONION_TUNNEL_DATA = 564
    ONION_ERROR = 565
    ONION_COVER = 566
    # reserved for Onion until 599

    AUTH_SESSION_START = 600
    AUTH_SESSION_HS1 = 601
    AUTH_SESSION_INCOMING_HS1 = 602
    AUTH_SESSION_HS2 = 603
    AUTH_SESSION_INCOMING_HS2 = 604
    AUTH_LAYER_ENCRYPT = 605
    AUTH_LAYER_DECRYPT = 606
    AUTH_LAYER_ENCRYPT_RESP = 607
    AUTH_LAYER_DECRYPT_RESP = 608
    AUTH_SESSION_CLOSE = 609
    AUTH_ERROR = 610
    # reserved for Onion Auth until 649


class MsgFixedSize(aenum.IntEnum, settings=(aenum.NoAlias,)):
    """Class used as alias for the APIProtocol
        messages with fixed sizes.
    """
    NSE_QUERY = 4
    NSE_ESTIMATE = 12
    GOSSIP_NOTIFY = 8
    GOSSIP_VALIDATION = 8
    RPS_QUERY = 4
    ONION_TUNNEL_DESTROY = 4
    ONION_ERROR = 12
    ONION_COVER = 4
    AUTH_SESSION_CLOSE = 8
    AUTH_ERROR = 12


class MsgMinSize(aenum.IntEnum, settings=(aenum.NoAlias,)):
    """Class used as alias for the minimum size the massages must have or they must be invalid."""
    GOSSIP_NOTIFY = 8
    GOSSIP_ANNOUNCE = 8
    GOSSIP_VALIDATION = 8
    GOSSIP_NOTIFICATION = 8
    NSE_QUERY = 4
    NSE_ESTIMATE = 12
    RPS_QUERY = 4
    RPS_PEER = 12
    RPS_PEER_IPV4 = 12
    RPS_PEER_IPV6 = 24
    ONION_TUNNEL_BUILD_IPV4 = 12
    ONION_TUNNEL_BUILD_IPV6 = 24
    ONION_TUNNEL_READY = 4
    ONION_TUNNEL_INCOMING = 4
    ONION_TUNNEL_DATA = 4
    ONION_TUNNEL_DESTROY = 4
    ONION_ERROR = 12
    ONION_COVER = 4
    AUTH_SESSION_START = 12
    AUTH_SESSION_HS1 = 12
    AUTH_SESSION_INCOMING_HS1 = 14
    AUTH_SESSION_HS2 = 12
    AUTH_SESSION_INCOMING_HS2 = 12
    AUTH_LAYER_ENCRYPT = 16
    AUTH_LAYER_DECRYPT = 16
    AUTH_LAYER_ENCRYPT_RESP = 12
    AUTH_LAYER_DECRYPT_RESP = 12
    AUTH_SESSION_CLOSE = 8
    AUTH_ERROR = 12


class RPSPeerFlags(aenum.IntFlag):
    """Class used to set/verify the
        IPv6 bit in the RPSPeer message.
    """
    # rest reserved
    ipv6 = 0x0001

    def __len__(self):
        """length of the reserved field"""
        return 2  # bytes


class GossipValidationFlags(aenum.IntFlag):
    """Class used to set/verify the
        well-formed bit in the
        GossipValidation message.
    """
    # rest reserved
    wellformed = 0x0001

    def __len__(self):
        """length of the reserved field"""
        return 2  # bytes
