#!/usr/bin/python3

import asyncio
import collections
import logging
import os
import signal
import sys
import time
from concurrent.futures import CancelledError
from ipaddress import IPv4Address, ip_address
from logging import critical, debug, error, info, warning
from typing import Any, Callable, List, Optional, Union, cast

import click
import toml

from p2psec.rps.api.client import (gossip_announce, gossip_notify,
                                   notify_rps_hello, nse_query, rps_hello)
from p2psec.rps.api.server import APIServer
from p2psec.rps.brahms import Brahms
from p2psec.rps.globals import *
from p2psec.rps.globals import DERCert, IPAddress, Port, rps_config
from p2psec.rps.p2p.client import ping, pull, push
from p2psec.rps.p2p.peer_protocol import PeerProtocol
from p2psec.rps.p2p.server import P2PServer
from p2psec.rps.peer import Peer

ClickCallback = Callable[  # pylint: disable=invalid-name
    [click.Context, click.Parameter, str], None]

LOG_LEVELS = ("notset", "debug", "info", "warning", "error", "critical")

api_server = None  # type: APIServer


def update_nested_dicts(base: dict, updated: collections.Mapping) -> dict:
    """Updates also dicts in dicts."""
    for key, val in updated.items():
        if isinstance(val, collections.Mapping):
            tmp = update_nested_dicts(base.get(key, {}), val)
            base[key] = tmp
        else:
            base[key] = updated[key]
    return base


def cli_config(key: str, type_: object) -> ClickCallback:
    """Overwrite the global rps configuration from the config file with the command line option values.

    Args:
        key: the name of the entry in the configuration with path (dot separated)
        type_: Reference to a python class. The updated value get converted to that class trough its constructor.
    """
    global rps_config  # pylint: disable=invalid-name
    return cli_config_dict(key, type_, rps_config)


def cli_config_dict(key: str, type_: Any, dict_: dict) -> ClickCallback:
    """Overwrite the dict `dict_` entry `key` with a new value.

    Args:
        key: the name of the entry in the configuration with path (dot separated)
        type_: Reference to a python class. The updated value get converted to that class trough its constructor.
        dict_: The dict to which the value gets written.
    """
    # pylint: disable=unused-argument
    keys = key.split('.')

    def cli_config_cb(ctx: click.Context, param: click.Parameter,
                      value: str) -> None:
        nonlocal keys
        nonlocal dict_
        nonlocal type_
        if value is not None:
            for k in keys[:-1]:
                tmp = dict_.get(k, dict())
                dict_[k] = tmp
                dict_ = tmp
            k = keys[-1]
            dict_[k] = type_(value)

    return cli_config_cb


def validate_verbosity(ctx: click.Context, param: click.Parameter,
                       value: str) -> Optional[int]:
    """Parse the verbosity from the command line."""
    # pylint: disable=unused-argument
    if value is None:
        return None
    ret = None

    try:
        ret = int(value)
    except (TypeError, ValueError):
        try:
            # we directly try to get the level number instead going trough
            # log_levels so there could be more valid ones then in log_levels
            ret = int(getattr(logging, value.upper()))
        except (AttributeError, TypeError):
            raise click.BadParameter("must be a number or one of " +
                                     ", ".join(LOG_LEVELS))

    return ret


@click.group()
@click.option("--verbosity", callback=validate_verbosity,
              help="Default is warning.")
@click.option("--verbose", "-v", count=True,
              help="Be more verbose (level -5). Can be repeated.")
@click.option("--quiet", "-q", count=True,
              help="Be more quiet (level +5). Can be repeated.")
@click.option("--config", "-c", type=click.Path(exists=True),
              help="Path to configuration file.")
@click.option("--api-server-listen",
              callback=cli_config("api.server.listen", lambda x: x.split(',')),
              help="A list of addresses the API Server will listen on.")
@click.option("--api-server-port",
              callback=cli_config("api.server.port", int),
              help="The port the API server will listen on.")
@click.option("--p2p-server-listen",
              callback=cli_config("p2p.server.listen", lambda x: x.split(',')),
              help="A list of addresses the P2P Server will listen on.")
@click.option("--p2p-server-address",
              callback=cli_config("p2p.server.address", lambda x: x.split(',')),
              help=("The address which the P2P will announce to other. Its "
                    "the address others will use to connect to RPS module and "
                    "therefore this address must be in the listen list."))
@click.option("--p2p-server-port",
              callback=cli_config("p2p.server.port", int),
              help="The port the P2P server will listen on.")
@click.option("--logfile", "-l",
              callback=cli_config("main.logfile", str),
              help=("the path to the file to which the module will log to. "
                    "Leave it emtpy to log to stdout."))
@click.option("--dercert", "-d",
              callback=cli_config("main.dercert", str),
              help=("The path to the DER encoded certificate the module will "
                    "onnounce to others."))
def cli(logfile: Optional[str] = None, verbosity: Optional[int] = None,
        verbose: Optional[int] = None, quiet: Optional[int] = None,
        config: Optional[str] = None, **kwargs) -> None:
    """Parsing of the command line arguments and initialization of the logger and its verbosity."""
    level_list = list()
    default_level = "WARNING"

    if config:
        global rps_config  # pylint: disable=invalid-name
        toml_config = toml.load(config)
        toml_config = update_nested_dicts(toml_config, rps_config)
        for key, val in toml_config.items():
            rps_config[key] = val

    log_to = dict()  # type: dict
    if rps_config["main"]["logfile"]:
        log_to = {"filename": rps_config["main"]["logfile"]}
    else:
        log_to = {"stream": sys.stdout}
    logging.basicConfig(format="%(asctime)-15s %(levelname)-7s %(name)-10s %("
                               "funcName)-16s %(message)s", **log_to)

    if verbosity:
        level = verbosity
        debug("Set log level to " + logging.getLevelName(level) + " (" + str(level) + ")")
        level_list.append(level)

    if verbose:
        level = getattr(logging, default_level) - (verbose * 5)
        debug("Set log level to " + logging.getLevelName(level) + " (" + str(level) + ")")
        level_list.append(level)

    if quiet:
        level = getattr(logging, default_level) + (quiet * 5)
        debug("Set log level to " + logging.getLevelName(level) + " (" + str(level) + ")")
        level_list.append(level)

    if len(level_list) > 1:
        warning("Multiple verbosity levels defined! Finally using " +
                logging.getLevelName(level_list[0]) + " (" + str(level_list[0]) + ").")

    level_list.append(getattr(logging, default_level))  # add default value

    logger = logging.getLogger()
    logger.setLevel(level_list[0])


@cli.command(help="Start the RPS module.")
def run() -> None:
    debug("Starting application.")
    loop = asyncio.get_event_loop()  # type: asyncio.AbstractEventLoop

    loop.add_signal_handler(signal.SIGTERM, cb_sigterm)
    loop.add_signal_handler(signal.SIGINT, cb_sigint)

    with open(rps_config["main"]["dercert"], 'rb') as f:
        dercert = f.read()

    # construct ourself as 'PeerProtocol'
    itselfip = ip_address(  # type:  ignore
        rps_config["p2p"]["server"]["address"])
    itself = PeerProtocol(hostkey=dercert,
                          ip=cast(IPAddress, itselfip),
                          rps_port=Port(rps_config["p2p"]["server"]["port"]),
                          onion_port=Port(rps_config["p2p"]["server"][
                                              "onion_port"]))

    # define callbacks
    nse = nse_query(rps_config["api"]["nse"]["address"],
                    rps_config["api"]["nse"]["port"], loop)
    notify_hello = notify_rps_hello(rps_config["api"]["gossip"]["address"],
                                    rps_config["api"]["gossip"]["port"], loop)

    send_rps_hello = rps_hello(rps_config["api"]["gossip"]["address"],
                               rps_config["api"]["gossip"]["port"], loop, itself=itself)

    pushed_peers_list = list()  # type: List[PeerProtocol]

    # start api + p2p server
    # The only thing they should do for now is to received pushes for the initial peer list and
    # to receive the RPSHello from Gossip
    api_server = APIServer(loop, rps_config["api"]["server"]["listen"],
                           rps_config["api"]["server"]["port"], brahms=None,
                           itself=itself)
    debug("started APIServer")
    p2p_server = P2PServer(loop, rps_config["p2p"]["server"]["listen"],
                           rps_config["p2p"]["server"]["port"],
                           pushed_server=pushed_peers_list,
                           brahms=None)
    debug("started P2PServer")

    # get an initial estimation of the network
    try:
        estimate_peers, estimate_std_dev = loop.run_until_complete(nse)
    except ConnectionRefusedError:
        msg = ("Refused NSE connection to {:s}:{:d}".format(rps_config["api"]["nse"]["address"],
                                                            rps_config["api"]["nse"]["port"]))
        error(msg)

    # now start registering RPSHello and send RPSHello to ger initial peers
    try:
        loop.run_until_complete(notify_hello)
        loop.run_until_complete(send_rps_hello)

    except ConnectionRefusedError:
        msg = ("Refused Gossip connection to {:s}:{:d}".format(rps_config["api"]["gossip"]["address"],
                                                               rps_config["api"]["gossip"]["port"]))
        error(msg)

    # we wait until we receive some pushes and therfore have some initial peers for brahms

    async def wait_for_initial_peers() -> None:
        wait_time = 20
        try:
            while True:
                await asyncio.sleep(wait_time)
                # if loop.is_closed():
                #     print("killed")
                #     # happens when program is killed by user
                #     return
                if len(pushed_peers_list) >= 2:
                    info("Received {:d} peers from RPS Hello.".format(len(pushed_peers_list)))
                    break
                else:
                    info("Still not enough peers received, just {:d} peers from RPS Hello. "
                         "Waiting additional {:d} seconds.".format(len(pushed_peers_list), wait_time))
        except CancelledError:
            return

    try:
        # TODO there is a "RuntimeError: Event loop stopped before Future completed." when stopping, but unknown why.
        loop.run_until_complete(wait_for_initial_peers())
    except CancelledError:
        return

    # now that we have initial peers, lets start brahms and therefore the real program

    nseip = ip_address(rps_config["api"]["nse"]["address"])  # type:  ignore
    brahms = Brahms(
        loop=loop,
        pushed_proportion=rps_config["brahms"]["pushed_proportion"],
        pulled_proportion=rps_config["brahms"]["pulled_proportion"],
        history_proportion=rps_config["brahms"]["history_proportion"],
        # TODO replace by real value
        # network_size=rps_config["brahms"]["network_size"],
        network_size=estimate_peers,
        ping_timeout=rps_config["brahms"]["ping_timeout"],
        heartbeat_round_time=rps_config["brahms"]["heartbeat_round_time"],
        initial_peers=pushed_peers_list,
        pushed_server=pushed_peers_list,
        brahms_round_time=rps_config["brahms"]["brahms_round_time"],
        itself=itself,
        nse_ip=cast(IPAddress, nseip),
        nse_port=rps_config["api"]["nse"]["port"]
    )

    del pushed_peers_list[:]

    api_server.brahms = brahms
    p2p_server.brahms = brahms

    time.sleep(8)

    # start brahms tasks
    heartbeat_task = asyncio.ensure_future(brahms.heartbeat(), loop=loop)
    brahms_task = asyncio.ensure_future(brahms.brahms(), loop=loop)

    try:
        loop.run_forever()

    finally:
        info("do close")
        for task in asyncio.Task.all_tasks():
            task.cancel()
        heartbeat_task.cancel()
        brahms_task.cancel()

        api_server.close()
        p2p_server.close()

        api_server.wait_closed()
        p2p_server.wait_closed()
        loop.close()


@cli.command(help="TEST function, It do not use Gossip/NSE.")
def noexternal():
    """Test Function that does the same as run() but without connecting to Gossip or NSE"""
    debug("Starting application.")
    loop = asyncio.get_event_loop()  # type: asyncio.AbstractEventLoop

    loop.add_signal_handler(signal.SIGTERM, cb_sigterm)
    loop.add_signal_handler(signal.SIGINT, cb_sigint)

    with open(rps_config["main"]["dercert"], 'rb') as f:
        dercert = f.read()

    # construct ourself as 'PeerProtocol'
    itselfip = ip_address(rps_config["p2p"]["server"]["address"])  # type:  ignore

    itself = PeerProtocol(hostkey=dercert,
                          ip=cast(IPAddress, itselfip),
                          rps_port=Port(rps_config["p2p"]["server"]["port"]),
                          onion_port=Port(rps_config["p2p"]["server"][
                                              "onion_port"]))

    # we have a predefined set of initial peers. After getting the initial peers, gossip is not needed any more.
    p1 = PeerProtocol(ip=IPv4Address("127.0.0.1"), onion_port=Port(3331),
                      rps_port=Port(4441), hostkey=DERCert(bytes([0x11] * 7)))
    p2 = PeerProtocol(ip=IPv4Address("127.0.0.1"), onion_port=Port(3332),
                      rps_port=Port(4442), hostkey=DERCert(bytes([0x22] * 7)))
    p3 = PeerProtocol(ip=IPv4Address("127.0.0.1"), onion_port=Port(3333),
                      rps_port=Port(4443), hostkey=DERCert(bytes([0x33] * 7)))

    if rps_config["p2p"]["server"]["port"] == 4441:
        initial_peers = [p2, p3]
        rps_config["api"]["server"]["port"] = 3331
    elif rps_config["p2p"]["server"]["port"] == 4442:
        initial_peers = [p1, p3]
        rps_config["api"]["server"]["port"] = 3332
    elif rps_config["p2p"]["server"]["port"] == 4443:
        initial_peers = [p1, p2]
        rps_config["api"]["server"]["port"] = 3333
    else:
        print("Wrong port for test")
        return None

    pushed_peers_list = initial_peers  # type: List[PeerProtocol]

    # start api + p2p server
    # The only thing they should do for now is to received pushes for the initial peer list and
    # to receive the RPSHello from Gossip
    api_server = APIServer(loop, rps_config["api"]["server"]["listen"],
                           rps_config["api"]["server"]["port"], brahms=None,
                           itself=itself)
    debug("started APIServer")
    p2p_server = P2PServer(loop, rps_config["p2p"]["server"]["listen"],
                           rps_config["p2p"]["server"]["port"],
                           pushed_server=pushed_peers_list,
                           brahms=None)
    debug("started P2PServer")

    # get an initial estimation of the network
    estimate_peers, estimate_std_dev = (40, 2)

    # now start registering RPSHello and send RPSHello to ger initial peers
    # --removed--

    # now that we have initial peers, lets start brahms and therefore the real program

    nseip = ip_address(rps_config["p2p"]["server"]["address"])  # type:  ignore
    brahms = Brahms(
        loop=loop,
        pushed_proportion=rps_config["brahms"]["pushed_proportion"],
        pulled_proportion=rps_config["brahms"]["pulled_proportion"],
        history_proportion=rps_config["brahms"]["history_proportion"],
        network_size=estimate_peers,
        ping_timeout=rps_config["brahms"]["ping_timeout"],
        heartbeat_round_time=rps_config["brahms"]["heartbeat_round_time"],
        initial_peers=pushed_peers_list,
        pushed_server=pushed_peers_list,
        brahms_round_time=rps_config["brahms"]["brahms_round_time"],
        itself=itself,
        nse_ip=None,  # we deactivate looking for an estimation (brahms will use an fixed size value)
        nse_port=rps_config["api"]["nse"]["port"]
    )

    del pushed_peers_list[:]

    api_server.brahms = brahms
    p2p_server.brahms = brahms

    time.sleep(8)

    # start brahms tasks
    heartbeat_task = asyncio.ensure_future(brahms.heartbeat(), loop=loop)
    brahms_task = asyncio.ensure_future(brahms.brahms(), loop=loop)

    try:
        loop.run_forever()

    finally:
        info("do close")
        for task in asyncio.Task.all_tasks():
            task.cancel()
        heartbeat_task.cancel()
        brahms_task.cancel()

        api_server.close()
        p2p_server.close()

        api_server.wait_closed()
        p2p_server.wait_closed()
        loop.close()


def cb_sigterm() -> None:
    """Handles a good termination of the program, in case that `SIGTERM` was received"""
    loop = asyncio.get_event_loop()
    warning("SIGTERM received. Shutting down application.")
    loop.stop()


def cb_sigint() -> None:
    """Handles a good termination of the program, in case that `SIGINT` was received"""
    loop = asyncio.get_event_loop()
    warning("SIGINT received. Shutting down application.")
    loop.stop()


if __name__ == "__main__":
    cli()
