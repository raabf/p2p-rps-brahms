"""The gobal configuration and some type required by all packages"""
import array
import logging
from ipaddress import IPv4Address, IPv6Address
from typing import Callable, NewType, Union

import click

rps_config = dict()  # type: dict  # pylint: disable=invalid-name
"""Configuration provided by command line or config file."""

# ## Types

ClickCallback = Callable[  # pylint: disable=invalid-name
    [click.Context, click.Parameter, str], None]

BytesLike = Union[bytes, bytearray, array.array, memoryview]
BytesType = Union[bytes, bytearray]
BytesSize = int
"""Some size in the unit bytes."""
IPAddress = Union[IPv4Address, IPv6Address]

Port = NewType("Port", int)
"""A network port."""
Automatic = None
"""Used when something can be automatically calculated or set. But the user can still set the value manually."""
DERCert = NewType("DERCert", bytes)
"""Big-Endian (Network Bytes order) certificate in DER form"""
