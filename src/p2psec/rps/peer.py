import hashlib
from typing import Optional

from p2psec.rps.globals import DERCert, IPAddress, Port


class Peer(object):
    """Representing all information known about a peer in the network."""
    def __init__(self, hostkey: DERCert, ip: Optional[IPAddress] = None,
                 onion_port: Optional[Port] = None,
                 rps_port: Optional[Port] = None) -> None:
        """Store all peer information.

        Args:
            hostkey: The public certificate in binary DER format.
            ip: The public ip of the peer.
            onion_port: The lisenig port of the Onion module.
            rps_port: The lisenig port of the RPS module.
        """
        super(Peer, self).__init__()
        self.ip = ip
        self.onion_port = onion_port
        self.rps_port = rps_port
        self.hostkey = hostkey

    def __str__(self) -> str:
        md5digest = hashlib.sha1(self.hostkey).hexdigest()
        md5digest = ':'.join(
            md5digest[i:i + 2] for i in range(0, len(md5digest), 2))
        return ("Peer({:s}, "
                "onion {:d}, "
                "rps {:d}, "
                "SHA1:{:s})").format(
            str(self.ip),
            self.onion_port,
            self.rps_port,
            md5digest
        )

    def __repr__(self) -> str:
        return ("P2PNode(ip = {:s}, "
                "onion_port = {:d}, "
                "rps_port = {:d}, "
                "hostkey = 0x{:s})").format(
            str(self.ip),
            self.onion_port,
            self.rps_port,
            self.hostkey.hex()
        )
