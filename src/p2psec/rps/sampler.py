import itertools
import os
import random
from abc import ABCMeta, abstractclassmethod
from typing import Any, Iterable, Iterator, List, Optional, cast

import xxhash

from p2psec.rps.globals import BytesLike, BytesType


class SamplerBlock(object, metaclass=ABCMeta):
    """A superclass for a Block in a Sampler. """

    def __init__(self, no: int) -> None:
        """A new empty Block.

        Args:
            no: A consecutively number of the block inside of the Sampler. It is just used to differentiate the Blcoks.
        """
        self._seed = None  # type: int
        self._no = no
        self.init()

    def init(self) -> None:
        """Reinitialize the Block without creating a new object. (Prefer this method)"""
        self._seed = int.from_bytes(os.urandom(64 // 8), "big", signed=False)

    def hash(self, elem: BytesLike) -> BytesType:
        """Hash the element with a per Block seed,"""
        # digest() returns a big-endian bytes object
        return cast(BytesType, xxhash.xxh64(elem, seed=self._seed).digest())

    @abstractclassmethod
    def next(self, elem) -> None:
        """Add a new element to this block. It depens on the hash if the current sample is replaced or not."""
        pass

    @abstractclassmethod
    def sample(self) -> Optional[Any]:
        """Returns the current sample of the block."""
        pass

    @property
    def no(self) -> int:
        """Get the number of the block inside the Sampler."""
        return self._no

    def __str__(self) -> str:
        return "SamplerBlock No.{:d} ({:s})".format(
            self.no, ("dead" if self.sample is None else "alive"))

    def __repr__(self) -> str:
        return "SamplerBlock No.{:d}: {:s}".format(self.no, repr(self.sample))


class Sampler(object):
    """A sampler which is able to provide a history of elements."""

    def __init__(self, amount: int, attribute: Optional[str] = None) -> None:
        """Initialize multiple sampler blocks.

        Args:
            amount: The amount of SamplerBlocks and therfore the amount of stored elements.
            attribute: If the element is an object where a member and not the object itself is the Identifier, the
                identifying attribute of the object can be specified here. Then the attribute will be hashed and not
                the object. The sampler the returns the entire object and not only the identifier attribute.
        """
        if amount < 1:
            raise ValueError("the amount must be greater one!")

        self.__secure_random = random.SystemRandom()
        self.__attribute = attribute
        super(Sampler, self).__init__()

        self._sbs = None  # type: List[SamplerBlock]
        """SamplerBlocks"""

        if attribute is None:
            self._sbs = list(SamplerBlockBytes(i) for i in range(
                amount))  # type: Iterator[SamplerBlock]
        else:
            self._sbs = list(SamplerBlockObject(i, attribute) for i in range(
                amount))

    def __getitem__(self, idx: int) -> SamplerBlock:
        return self._sbs[idx]

    def update_samplers(self, elems: Iterable[Any]):
        """Given multiple probabliy new elements, `elems` are put into the SamplerBlocks.

        Args:
            elems: A sequence of probably new elements.
        """
        for elem, sampler in itertools.product(elems, self._sbs):
            sampler.next(elem)

    def rand_samples(self, k: int):
        """Returns k cryptographically secure random elements from the SamplerBlocks"""
        return self.__secure_random.sample(
            [sb.sample for sb in self._sbs if sb.sample is not None], k
        )

    def __str__(self) -> str:
        alive = 0
        dead = 0
        for sb in self._sbs:
            if sb.sample is None:
                dead += 1
            else:
                alive += 1

        return ("Sampler: {:d} peers alive and {:d} dead of {:d} "
                "SamplerBlocks.".format(alive, dead, self.amount))

    def __repr__(self) -> str:
        return "Sampler:" + repr(self._sbs)

    @property
    def amount(self) -> int:
        """Get the amount of blocks."""
        return len(self._sbs)

    @amount.setter
    def amount(self, value: int) -> None:
        """Set a new amount of blocks.

        The blcoks will be automtically shrinked or expandend. Remember to call update_samplers() afterwards.
        """
        if value < self.amount:
            del self._sbs[value:]

        elif value > self.amount:

            if self.__attribute is None:
                self._sbs.extend(
                    SamplerBlockBytes(i) for i in range(self.amount, value))  # type: Iterator[SamplerBlock]
            else:
                self._sbs.extend(SamplerBlockObject(i, self.__attribute) for
                                 i in range(self.amount, value))


class SamplerBlockBytes(SamplerBlock):
    """A SamplerBlock able to store arbitrary bytes which is also the ID of the element."""
    def __init__(self, no: int) -> None:
        """

        Args:
            no: A consecutively number of the block inside of the Sampler. It is just used to differentiate the Blcoks.
        """
        super(SamplerBlockBytes, self).__init__(no)
        self._sample = None  # type: Optional[BytesLike]

    def init(self) -> None:
        super().init()
        self._sample = None

    def next(self, elem: BytesLike) -> None:
        # make sure hash() returns big endian for a correct comparison
        if self._sample is None or self.hash(self._sample) > self.hash(elem):
            self._sample = elem

    @property
    def sample(self) -> Optional[BytesLike]:
        return self._sample


class SamplerBlockObject(SamplerBlock):
    """A sampler block able to store any object, A attribute of the object is the ID of the element."""
    def __init__(self, no: int, attribute: str) -> None:
        """

        Args:
            no: A consecutively number of the block inside of the Sampler. It is just used to differentiate the Blcoks.
            attribute: An attribute of the object representing the ID of the element. The attribute will be hashed
                and not the object. The sampler the returns the entire object and not only the identifier attribute.
        """
        super(SamplerBlockObject, self).__init__(no)
        self._sample = None  # type: Optional[object]
        self._attribute = attribute

    def init(self) -> None:
        super().init()
        self._sample = None

    def next(self, elem: object) -> None:
        attr = getattr(elem, self._attribute)
        # make sure hash() returns big endian for a correct comparison
        if self._sample is None or (self.hash(getattr(self._sample,
                                                      self._attribute
                                                      )) > self.hash(attr)):
            self._sample = elem

    @property
    def sample(self) -> Optional[object]:
        return self._sample
