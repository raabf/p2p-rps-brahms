from struct import Struct
from typing import List, Optional
from warnings import warn

from p2psec.rps.globals import Automatic, BytesSize, BytesType
from p2psec.rps.p2p.p2p_protocol import P2PProtocol
from p2psec.rps.p2p.peer_protocol import PeerProtocol


class PushChallenge(P2PProtocol):
    """Class for the PushChallenge packets.

    Handles the packing, unpacking and size calculation
    for the PushChallenge packets in P2P communication.
    """
    size_nonce_struct = Struct("!Hxx8s")
    resource_struct = Struct("!8s")
    difficulty_struct = Struct("!B")
    header_size = size_nonce_struct.size

    def __init__(self, size: Optional[BytesSize] = Automatic,
                 nonce: Optional[BytesType] = None,
                 resource: Optional[BytesType] = None,
                 difficulty: Optional[int] = 20) -> None:
        """Initializes the 'size', 'nonce', 'resource' and
            'difficulty' fields of the packet.

        Args:
            size: The value in the size field. If Automatic it
            will be calculated. Automatic is recommended.
            nonce: A random number which guarantees both
            the validity and freshness of the message.
            resource: A random number used as a 'challenge'
            to be hashed by the P2PClient.
            difficulty: Number of leading zeros, expressing
            the difficulty of the 'challenge'
        """
        super(PushChallenge, self).__init__(size)

        self.nonce = nonce  # type: BytesType
        self.resource = resource
        self.difficulty = difficulty

    @property
    def size(self) -> Optional[BytesSize]:
        size = 0

        size += self.size_nonce_struct.size
        size += self.resource_struct.size
        size += self.difficulty_struct.size

        if self._size and self._size != size:
            msg = ("Calculated size {:d} do not match size specified "
                   "{:d}!".format(size, self._size))
            warn(msg)
            self.log.error(msg)

        if self._size is not Automatic:
            return self._size
        else:
            return size

    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def pack(self) -> Optional[bytearray]:
        buffer = bytearray(self.size)
        self.pack_into(buffer, offset=0)
        return buffer

    def pack_into(self, buffer: bytearray, offset: BytesSize = 0) -> None:
        """Packs into an already existing `buffer` from the position `offset`.

        Args:
            buffer: An buffer with sufficient size.
            offset: Starting point to copy data into buffer.
        """
        if len(buffer) < self.size:
            msg = "Buffer is too small."
            self.log.error(msg)
            warn(msg)
            return

        pos = offset
        self.size_nonce_struct.pack_into(buffer, pos, self.size, self.nonce)
        pos += self.size_nonce_struct.size

        self.resource_struct.pack_into(buffer, pos, self.resource)
        pos += self.resource_struct.size

        self.difficulty_struct.pack_into(buffer, pos, self.difficulty)

    def unpack(self, buffer: BytesType, offset: BytesSize = 0) -> None:
        pos = offset
        self.unpack_header(buffer, offset=pos)

        pos += self.size_nonce_struct.size
        self.unpack_challenge(buffer, offset=pos)

    def unpack_header(self, buffer: BytesType, offset: BytesSize = 0) -> None:
        """Unpacks the header from an already existing `buffer`
            from the position `offset`.

        Args:
            buffer: A buffer containing the data.
            offset: Starting point to copy data from the buffer.
        """
        self.size, self.nonce = self.size_nonce_struct.unpack_from(
            buffer, offset=offset)

    def unpack_challenge(self, buffer: BytesType, offset: BytesSize = 0) -> None:
        """Unpacks the rest of the message from an already
            existing `buffer` from the position `offset`.

        Args:
            buffer: A buffer containing the data.
            offset: Starting point to copy data from the buffer.
        """
        pos = offset
        self.resource = self.resource_struct.unpack_from(buffer,
                                                         offset=pos)[0]

        pos += self.resource_struct.size
        self.difficulty = self.difficulty_struct.unpack_from(buffer,
                                                             offset=pos)[0]

    def __repr__(self):
        return (super(PushChallenge, self).__repr__() + ", nonce = 0x" +
                repr(self.nonce.hex()) + ", " + repr(self.resource.hex()) +
                ", " + repr(self.difficulty))

    def __str__(self):
        line1a = P2PProtocol.__str__(self)
        line1b = "{:^17s}|".format("")
        line2 = "|{:^35s}|".format("")
        line3 = "|{:^35s}|".format("0x" + self.nonce.hex())
        line4 = "|{:^35s}|".format("0x" + self.resource.hex())
        line5 = "|{:^35s}|".format(str(self.difficulty))

        out = line1a + line1b + "\n" + line2 + "\n" + line3 + "\n" + line4 + \
            "\n" + line5

        return out


class PushReply(P2PProtocol):
    """Class for the PushReply packets.

    Handles the packing, unpacking and size calculation
    for the PushReply packets in P2P communication.
    """
    size_nonce_struct = Struct("!Hxx8s")
    header_size = size_nonce_struct.size

    def __init__(self, size: Optional[BytesSize] = Automatic,
                 nonce: Optional[BytesType] = None,
                 answer: Optional[BytesType] = None,
                 peer: Optional[PeerProtocol] = None) -> None:
        """Initializes the 'size', 'nonce', 'answer' and
            'peer' fields of the packet.

        Args:
            size: The value in the size field. If Automatic it
            will be calculated. Automatic is recommended.
            nonce: A random number which guarantees both
            the validity and freshness of the message.
            answer: The hash of the 'resource' after
            solved by the challenged peer
            peer: A PeerProtocol object representing
            the sender.
        """
        super(PushReply, self).__init__(size)

        self.nonce = nonce  # type: BytesType
        self.answer = answer
        self.peer = peer

    @property
    def size(self) -> Optional[BytesSize]:
        size = 0
        is_calculable = True
        size += self.size_nonce_struct.size

        if self.peer is None or self.peer.size is None:
            is_calculable = False
        else:
            size += self.peer.size

        if self.answer is None:
            is_calculable = False
        else:
            size += len(self.answer)

        if self._size is not Automatic:
            return self._size
        elif is_calculable:
            return size
        else:
            msg = "Error calculating PushReply size"
            self.log.error(msg)
            warn(msg)
            return None

    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def pack(self) -> Optional[bytearray]:
        buffer = bytearray(self.size)
        self.pack_into(buffer, offset=0)
        return buffer

    def pack_into(self, buffer: bytearray, offset: BytesSize = 0) -> None:
        """Packs into an already existing `buffer` from the position `offset`.

        Args:
            buffer: An buffer with sufficient size.
            offset: Starting point to copy data into buffer.
        """
        if len(buffer) < self.size:
            msg = "Buffer is too small."
            self.log.error(msg)
            warn(msg)
            return

        pos = offset
        self.size_nonce_struct.pack_into(buffer, pos, self.size, self.nonce)

        pos += self.size_nonce_struct.size
        self.peer.pack_into(buffer, pos)

        pos += self.peer.size
        buffer[pos: pos + len(self.answer)] = self.answer

    def unpack(self, buffer: BytesType, offset: BytesSize = 0) -> None:
        pos = offset
        self.unpack_header(buffer, offset=pos)

        pos += self.size_nonce_struct.size
        self.unpack_reply(buffer, offset=pos)

    def unpack_header(self, buffer: BytesType, offset: BytesSize = 0) -> None:
        """Unpacks the header from an already existing `buffer`
            from the position `offset`.

        Args:
            buffer: A buffer containing the data.
            offset: Starting point to copy data from the buffer.
        """
        self.size, self.nonce = self.size_nonce_struct.unpack_from(
            buffer, offset=offset)

    def unpack_reply(self, buffer: BytesType, offset: BytesSize = 0) -> None:
        """Unpacks the rest of the message from an already
            existing `buffer` from the position `offset`.

        Args:
            buffer: A buffer containing the data.
            offset: Starting point to copy data from the buffer.
        """
        pos = offset
        pp = PeerProtocol()
        pp.unpack(buffer, offset=pos)
        self.peer = pp

        pos += self.peer.size
        self.answer = buffer[pos:offset + self._size]

    def __repr__(self):
        return (super(PushReply, self).__repr__() + ", nonce = 0x" +
                repr(self.nonce.hex()) + ", " + repr(self.answer.hex()) +
                ", " + repr(self.peer))

    def __str__(self):
        line1a = P2PProtocol.__str__(self)
        line1b = "{:^17s}|".format("")
        line2 = "|{:^35s}|".format("")
        line3 = "|{:^35s}|".format("0x" + self.nonce.hex())
        line4 = "|{:^35s}|".format("0x" + self.answer[0:15].hex() + "...")

        out = line1a + line1b + "\n" + line2 + "\n" + line3 + "\n" + line4 + \
            "\n" + str(self.peer)

        return out
