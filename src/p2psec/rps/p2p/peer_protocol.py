from ipaddress import IPv4Address, IPv6Address
from struct import Struct
from typing import Optional
from warnings import warn

from p2psec.rps.globals import (
    Automatic, BytesSize, BytesType, DERCert, IPAddress, Port)
from p2psec.rps.p2p.p2p_protocol import P2PProtocol
from p2psec.rps.peer import Peer


class PeerProtocol(Peer, P2PProtocol):
    """Representing all information known about a peer in the network with the ability to be packed.

    Subclass of Peer but also P2PProtocol.
    """
    size_ipv_struct = Struct("!HBx")
    ports_struct = Struct("!HH")

    def __init__(self, hostkey: Optional[DERCert] = None,
                 ip: Optional[IPAddress] = None,
                 onion_port: Optional[Port] = None,
                 rps_port: Optional[Port] = None,
                 size: Optional[BytesSize] = Automatic,
                 ipv: Optional[int] = Automatic) -> None:
        """Store all peer information in a serializable object.

        Args:
            hostkey: The public certificate in binary DER format.
            ip: The public ip of the peer.
            onion_port: The lisenig port of the Onion module.
            rps_port: The lisenig port of the RPS module.
            size: The value in the size field. If Automatic it will be calculated (see P2PProtocol.size()).
                Automatic is recommended.
            ipv: The IP version. Only v4 and v6 are supported. If Automatic it is determined from `ip`.
        """
        super(PeerProtocol, self).__init__(ip=ip, onion_port=onion_port,
                                           rps_port=rps_port, hostkey=hostkey)
        # self._size = None
        self.size = size
        self._ipv = None  # type: Optional[int]
        self.ipv = ipv

    @property
    def ipv(self) -> int:
        """Get the IP version in the ipv field.

        Only v4 and v6 are supported.

        Returns:
            If set to Automatic, the Ip version is detemined from the `ip` attribute.
            If set to an int, the int is returned.
        """
        if self._ipv is Automatic:
            if isinstance(self.ip, IPv4Address):
                return 4
            if isinstance(self.ip, IPv6Address):
                return 6
            raise NotImplementedError("Other IP versions then 4 or 6 are not "
                                      "supported.")
        else:
            return self._ipv

    @ipv.setter
    def ipv(self, value: Optional[int]) -> None:
        """Set the IP version in the ipv field.

        Only v4 and v6 are supported.

        Args:
            value: An int value or it can be set to Automatic.
        """
        self._ipv = value

    @property
    def size(self) -> BytesSize:
        size = 0
        size += self.size_ipv_struct.size
        size += self.ports_struct.size
        is_calculable = True

        if self.ipv == 4:
            size += 4
        elif self.ipv == 6:
            size += 16
        else:
            msg = ("Automatic size calculation for other IPVersions then "
                   "4 or 6 is not supported")
            self.log.error(msg)
            warn(msg)
            is_calculable = False

        if self.hostkey:
            size += len(self.hostkey)
        else:
            is_calculable = False

        if is_calculable and self._size and self._size != size:
            msg = ("Calculated size {:d} do not match size specified "
                   "{:d}!".format(size, self._size))
            warn(msg)
            self.log.error(msg)

        if self._size is not Automatic:
            return self._size
        elif is_calculable:
            return size
        else:
            msg = ("Unable to calculate the size or the size attribute is not "
                   "set.")
            self.log.error(msg)
            warn(msg)
            return None

    @size.setter
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def pack(self) -> Optional[bytearray]:
        buffer = bytearray(self.size)
        self.pack_into(buffer, offset=0)
        return buffer

    def pack_into(self, buffer: bytearray, offset: BytesSize = 0) -> None:
        """Packs into an already existing `buffer` from the position `offset`.

        Args:
            buffer: An buffer with sufficent size.
            offset: starting point to copy data into buffer.
        """
        pos = offset
        self.size_ipv_struct.pack_into(buffer, pos, self.size, self.ipv)

        pos += self.size_ipv_struct.size
        self.ports_struct.pack_into(buffer, pos,
                                    self.rps_port, self.onion_port)

        pos += self.ports_struct.size

        ip_packed = self.ip.packed
        if self.ipv == 4 and len(ip_packed) == 4:
            buffer[pos: pos + len(ip_packed)] = ip_packed
            pos += 4
        elif self.ipv == 6 and len(ip_packed) == 16:
            buffer[pos: pos + len(ip_packed)] = ip_packed
            pos += 16
        else:
            msg = "Other IP versions then 4 or 6 are not supported."
            self.log.error(msg)
            warn(msg)
            return

        if offset + self.size - pos < len(self.hostkey):
            msg = "The hostkey do not fit into the specified size."
            msg += str(self.size) + "  " + str(pos) + "   " + str(len(
                self.hostkey)) + "  " + str(offset)
            self.log.error(msg)
            warn(msg)
        else:
            buffer[pos:pos + len(self.hostkey)] = self.hostkey

    def unpack(self, buffer: BytesType, offset: BytesSize = 0) -> None:
        pos = offset
        self.size, self.ipv = self.size_ipv_struct.unpack_from(buffer,
                                                               offset=pos)

        pos += self.size_ipv_struct.size

        (self.rps_port,
         self.onion_port) = self.ports_struct.unpack_from(buffer, offset=pos)

        pos += self.ports_struct.size

        if self.ipv == 4:
            # TODO for some reason "buffer[pos: pos + 16]" needs to be converted
            # into bytes (not bytearray) or else IPv6/4Address will interpret it
            # as string or something else (The error says he expect :). Is
            # probably a bug in ipaddress library.
            self.ip = IPv4Address(bytes(buffer[pos: pos + 4]))
            pos += 4
        elif self.ipv == 6:
            self.ip = IPv6Address(bytes(buffer[pos: pos + 16]))
            pos += 16
        else:
            msg = "Other IP versionons then 4 or 6 are not supported."
            self.log.error(msg)
            warn(msg)
            return None

        self.hostkey = DERCert(buffer[pos:offset + self.size])

    def clean(self) -> None:
        """Set members to Automatic which support this."""
        size_tmp = self.size
        self.size = Automatic

        # ipv_tmp = self.ipv
        self.ipv = Automatic

        if self.size != size_tmp:
            msg = "calculated size does not match stored size. Aborting clean."
            self.log.error(msg)
            warn(msg)

    def __repr__(self) -> str:
        return (P2PProtocol.__repr__(self) + ", IPv" + repr(self.ipv) + ", " +
                super(PeerProtocol, self).__repr__())

    def __str__(self) -> str:
        line1a = P2PProtocol.__str__(self)
        line1b = "{:^8s}|{:^8s}|".format("IPv" + str(self.ipv), "")
        line2 = "|{:^17d}|{:^17d}|".format(self.rps_port, self.onion_port)
        line3 = "/{:^35s}/".format(str(self.ip))
        line4 = "/{:^35s}/".format("0x" + str(self.hostkey[0:15].hex()) + "...")

        return line1a + line1b + "\n" + line2 + "\n" + line3 + "\n" + line4
