from struct import Struct
from typing import List, Optional
from warnings import warn

from p2psec.rps.globals import Automatic, BytesSize, BytesType
from p2psec.rps.p2p.p2p_protocol import P2PProtocol
from p2psec.rps.p2p.peer_protocol import PeerProtocol


class PullReply(P2PProtocol):
    """Class for the PullReply packets.

    Handles the packing, unpacking and size calculation
    for the PullReply packets in P2P communication.
    """
    size_nonce_struct = Struct("!Hxx8s")
    header_size = size_nonce_struct.size
    """The size until the list of peers begin"""

    def __init__(self, size: Optional[BytesSize] = Automatic,
                 nonce: Optional[BytesType] = None,
                 peers: List[PeerProtocol] = None) -> None:
        """Initializes the 'size', 'nonce' and
            'peers' fields of the packet.

        Args:
            size: The value in the size field. If Automatic it
            will be calculated. Automatic is recommended.
            nonce: A random number which guarantees both
            the validity and freshness of the message.
            peers: A list containing PeerProtocol objects,
             containing the sender's localview peer's data.
        """
        super(PullReply, self).__init__(size)
        self.peers = list() if peers is None else peers
        self.nonce = nonce  # type: BytesType

    @property
    def size(self) -> Optional[BytesSize]:
        size = 0
        is_calculable = True
        size += self.size_nonce_struct.size

        for peer in self.peers:
            peer_size = peer.size
            if peer_size is None:
                is_calculable = False
                break
            size += peer_size

        if self._size is not Automatic:
            return self._size
        elif is_calculable:
            return size
        else:
            msg = ("One of the peers in the list do not have his size set "
                   "and therefore unable to calculate size.")
            self.log.error(msg)
            warn(msg)
            return None

    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def pack(self) -> Optional[bytearray]:
        buffer = bytearray(self.size)
        self.pack_into(buffer, offset=0)
        return buffer

    def pack_into(self, buffer: bytearray, offset: BytesSize = 0) -> None:
        """Packs into an already existing `buffer` from the position `offset`.

        Args:
            buffer: An buffer with sufficient size.
            offset: Starting point to copy data into buffer.
        """
        # access of self.size also checks if every peer in the list has a size
        if len(buffer) < self.size:
            msg = "Buffer is too small."
            self.log.error(msg)
            warn(msg)
            return

        pos = offset
        self.size_nonce_struct.pack_into(buffer, pos, self.size, self.nonce)
        pos += self.size_nonce_struct.size

        for peer in self.peers:
            peer.pack_into(buffer, pos)
            pos += peer.size

    def unpack(self, buffer: BytesType, offset: BytesSize = 0) -> None:
        pos = offset
        self.unpack_header(buffer, offset=pos)

        pos += self.size_nonce_struct.size
        self.unpack_peers(buffer, offset=pos)

    def unpack_header(self, buffer: BytesType, offset: BytesSize = 0) -> None:
        """Unpacks the header from an already existing `buffer`
            from the position `offset`.

        Args:
            buffer: A buffer containing the data.
            offset: Starting point to copy data from the buffer.
        """
        self.size, self.nonce = self.size_nonce_struct.unpack_from(
            buffer, offset=offset)

    def unpack_peers(self, buffer: BytesType, offset: BytesSize = 0) -> None:
        """Unpacks the rest of the message from an already
            existing `buffer` from the position `offset`.

        Args:
            buffer: A buffer containing the data.
            offset: Starting point to copy data from the buffer.
        """
        pos = offset
        while pos < offset + self.size - self.header_size:
            pp = PeerProtocol()
            pp.unpack(buffer, offset=pos)
            pos += pp.size
            self.peers.append(pp)

    def __repr__(self):
        return (super(PullReply, self).__repr__() + ", nonce = 0x" +
                repr(self.nonce.hex()) + ", " + repr(self.peers))

    def __str__(self):
        line1a = P2PProtocol.__str__(self)
        line1b = "{:^17s}|".format("")
        line2 = "|{:^35s}|".format("")
        line3 = "|{:^35s}|".format("0x" + self.nonce.hex())

        out = line1a + line1b + "\n" + line2 + "\n" + line3

        for peer in self.peers:
            out += "\n" + str(peer)

        return out
