import logging
import os
from abc import ABCMeta, abstractclassmethod
from enum import IntEnum
from struct import Struct
from typing import Optional
from warnings import warn

import aenum

from p2psec.rps.globals import Automatic, BytesSize, BytesType


class RPSMsgFixedSize(aenum.IntEnum, settings=(aenum.NoAlias,)):
    """Class used as alias for the RPS messages with fixed sizes
    """
    REQUEST = 12
    PONG = 12


class P2PProtocol(metaclass=ABCMeta):
    """Base Class for all protocols used in the P2P
     communication of the RPS module.

    Handles the size field, which all have in common.
    """
    size_struct = Struct("!H")

    def __init__(self, size: Optional[BytesSize] = Automatic) -> None:
        """Initializes the first field size.

        Args:
            size: The value in the size field. If Automatic it will be
            calculated (see P2PProtocol.size()). Automatic is recommended.
        """
        super(P2PProtocol, self).__init__()
        self._size = None  # type: Optional[BytesSize]
        self.size = size  # type: Optional[BytesSize]
        self.log = logging.getLogger("P2PProtocol")

    # Those two classes must be implemented in subclasses
    # (@abstractclassmethod can't be uses with @property)

    # @abstractclassmethod
    @property
    def size(self) -> BytesSize:
        """Calculates the size of the packet.

        Returns:
            if size is Automatic: The total size will be calculated based on the
            other attributes. Maybe returns a None if the size is not calculable
            cause too many attributes are not set.
            if size is an int: It returns just the size field, but throws a
            warning if the calculated size do not match the size field.
        """
        raise NotImplementedError("size getter not implemented.")

    # @abstractclassmethod
    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)
        raise NotImplementedError("size setter not implemented.")

    def _set_size(self, value: Optional[BytesSize]) -> None:
        """Sets the size field.

        Use P2PProtocol.size instead of this method. This method is just
        required, because setter cannot be inherited.
        """
        self._size = value

    @abstractclassmethod
    def pack(self) -> BytesType:
        """Packs the object into bytes suitable for network transmission.

        Returns:
            Plain bytes in encoded according to the specification.
        """
        pass

    @abstractclassmethod
    def unpack(self, buffer: BytesType, offset: BytesSize) -> None:
        """Unpacks the bytes into the members of the object.

        Postcondition:
            All members of the class must be set.

        Args:
            buffer: A buffer of length size.
            offset: of the buffer
        """
        pass

    def __repr__(self) -> str:
        if isinstance(self.size, IntEnum):
            return (repr(self.__class__.__name__) + ": size = " + repr(int(
                self.size)) + " (" + repr(self.size) +
                    ") bytes")
        else:
            return (repr(self.__class__.__name__) + ": size = " +
                    repr(int(self.size)) + " bytes")

    def __str__(self) -> str:
        header = "|{:<4d}{:>4d}|{:<4d}{:>4d}|{:<4d}{:>4d}|{:<4d}{:>4d}|".format(
            0, 7, 8, 15, 16, 23, 24, 31
        )
        sep = "|{:-^35s}|".format('')
        line = "|{:^17d}|".format(self.size)
        return header + '\n' + sep + "\n" + str(line)


class RequestType(aenum.IntEnum):
    """Class used as enumeration to distinguish
        between requests received in the P2PProtocol.
    """
    EMPTY = 0
    PING = 1
    PUSH = 2
    PULL = 3


class Request(P2PProtocol):
    """Class for the Request packets.

    Handles the packing, unpacking and size calculation
    for the Request packets in P2P communication.
    """
    request_struct = Struct("!HxB8s")

    def __init__(self, size: Optional[BytesSize] = RPSMsgFixedSize.REQUEST,
                 request_type: int = RequestType.EMPTY,
                 nonce: Optional[BytesType] = Automatic) -> None:
        """Initializes the 'size', 'request_type' and
            'nonce' fields of the packet.

        Args:
            size: The value in the size field. If Automatic it
            will be calculated. Automatic is recommended.
            request_type: Type of the request received.
            nonce: A random number which guarantees both
            the validity and freshness of the message.
        """
        super(Request, self).__init__(size)
        self.rtype = request_type
        self._nonce = None  # type: Optional[BytesType]
        self.nonce = nonce  # type: BytesType

    @property
    def nonce(self) -> BytesType:
        """Calculates the nonce of the packet.

        Returns:
            If the nonce is not set, generates a 8-bit
            random number to use as a nonce in the packet.
        """
        if self._nonce is None:
            self._nonce = os.urandom(8)
        return self._nonce

    @nonce.setter  # type: ignore
    def nonce(self, value: BytesType):
        self._nonce = value

    def pack(self) -> BytesType:
        if self.rtype == RequestType.EMPTY:
            warn("Request type is not set! (== RequestType.EMPTY)")
        try:
            RequestType(self.rtype)
        except ValueError:
            warn("Request type is set to a not supported value: {}".format(
                self.rtype))

        packed = self.request_struct.pack(self.size, self.rtype, self.nonce)
        return packed

    def unpack(self, buffer: BytesType, offset: BytesSize) -> None:
        if len(buffer) < RPSMsgFixedSize.REQUEST:
            msg = ("Request unpack try with too small buffer: {},"
                   "expected length: {}".format(len(buffer),
                                                RPSMsgFixedSize.REQUEST))
            self.log.error(msg)
            warn(msg)

        self.size, rtype, self.nonce = self.request_struct.unpack_from(
            buffer, offset)

        try:
            self.rtype = RequestType(rtype)
        except ValueError:
            self.rtype = rtype

    @property
    def size(self) -> BytesSize:
        if self._size is Automatic:
            return self.request_struct.size
        else:
            return self._size

    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def __repr__(self) -> str:
        return (super(Request, self).__repr__() + ", request type = " +
                repr(self.rtype) + ", nonce = 0x" + repr(self.nonce.hex()))

    def __str__(self) -> str:
        line1 = super(Request, self).__str__()
        if isinstance(self.rtype, RequestType):
            rstr = self.rtype.name
        else:
            rstr = str(self.rtype)
        line1b = "{:^8s}|{:^8s}|".format("", rstr)
        line3 = "|{:^35s}|".format("0x" + self.nonce.hex())
        line2 = "|{:^35s}|".format('')
        return line1 + line1b + "\n" + line2 + "\n" + line3


class Pong(P2PProtocol):
    """Class for the Pong packets.

    Handles the packing, unpacking and size calculation
    for the Pong packets in P2P communication.
    """
    pong_struct = Struct("!Hxx8s")

    def __init__(self, size: Optional[BytesSize] = RPSMsgFixedSize.PONG,
                 nonce: Optional[BytesType] = None) -> None:
        """Initializes the 'size' and 'nonce' fields of the packet.

        Args:
            size: The value in the size field. If Automatic it
            will be calculated. Automatic is recommended.
            nonce: A random number which guarantees both
            the validity and freshness of the message.
        """
        super(Pong, self).__init__(size)
        self.nonce = nonce  # type: BytesType

    def pack(self) -> BytesType:
        return self.pong_struct.pack(self.size, self.nonce)

    def unpack(self, buffer: BytesType, offset: BytesSize) -> None:
        if len(buffer) < RPSMsgFixedSize.PONG:
            msg = ("Pong unpack try with too small buffer: {},"
                   "expected length: {}".format(len(buffer),
                                                RPSMsgFixedSize.PONG))
            self.log.error(msg)
            warn(msg)

        self.size, self.nonce = self.pong_struct.unpack_from(buffer, offset)

    @property
    def size(self) -> BytesSize:
        if self._size is Automatic:
            return self.pong_struct.size
        else:
            return self._size

    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def __repr__(self) -> str:
        return (super(Pong, self).__repr__() +
                ", nonce = 0x" + repr(self.nonce.hex()))

    def __str__(self) -> str:
        line1 = super(Pong, self).__str__()
        line1b = "{:^17s}|".format("")
        line3 = "|{:^35s}|".format("0x" + self.nonce.hex())
        line2 = "|{:^35s}|".format('')
        return line1 + line1b + "\n" + line2 + "\n" + line3
