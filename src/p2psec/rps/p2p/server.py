import asyncio
import logging
from concurrent.futures import CancelledError
from ipaddress import IPv4Address, IPv6Address
from os import urandom
from typing import Iterable, List, Optional, cast

from p2psec.rps.brahms import Brahms
from p2psec.rps.globals import DERCert, Port
from p2psec.rps.p2p.hashcash import hashcash_check
from p2psec.rps.p2p.p2p_protocol import (
    Pong, Request, RequestType, RPSMsgFixedSize)
from p2psec.rps.p2p.p2p_pull import PullReply
from p2psec.rps.p2p.p2p_push import PushChallenge, PushReply
from p2psec.rps.p2p.peer_protocol import PeerProtocol
from p2psec.rps.peer import Peer


class P2PServer(object):
    """Class for the P2PServer.

    It is the server to connect between RPS modules of multiple peers in the network.
    """

    def __init__(self, loop: asyncio.AbstractEventLoop,
                 listen: Iterable[str], port: Port,
                 pushed_server: List[PeerProtocol],
                 brahms: Optional[Brahms]) -> None:
        """Initializes the server with loop, listen ip, port, brahms
            algorithm and a Peer Object.

        Args:
            loop: The event loop in which the server runs.
            listen: The IP address in which the server should listen on.
            port: The port in which the server should listen on.
            brahms: A reference to brahms. It can be None if brahms is not started yet. Add it later via attribure.
            pushed_server: A list where peers should be stored which are reveived by a push.
        """
        self._loop = loop
        self._log = logging.getLogger("P2P.Server")
        coro = asyncio.start_server(self.handle_connection,
                                    cast(str, listen), port, loop=loop)
        self._log.info("started at " + ", ".join(listen) + " on port " +
                       str(port) + ".")
        self.__server = loop.run_until_complete(coro)
        self.pushed_server = pushed_server
        self.brahms = brahms

    async def handle_connection(self, reader: asyncio.StreamReader,
                                writer: asyncio.StreamWriter):
        """Handles the connection established with the client and process its requests."""
        remote_ip, remote_port = writer.get_extra_info('peername')
        remote_addr = "{:s}:{:d}".format(str(remote_ip), remote_port)
        self._log.info("Connected to " + str(remote_addr))
        try:
            while True:
                data = await reader.read(RPSMsgFixedSize.REQUEST)
                # data = data.decode("utf-8")

                if not data:
                    self._log.info("Remote " + str(remote_addr) +
                                   " has closed the connection.")
                    await writer.drain()
                    return None

                req = Request()
                req.unpack(data, offset=0)

                if req.rtype == RequestType.PING:
                    await self.handle_ping(reader, writer, req)
                elif req.rtype == RequestType.PULL:
                    await self.handle_pull(reader, writer, req)
                elif req.rtype == RequestType.PUSH:
                    await self.handle_push(reader, writer, req)

        except CancelledError:
            self._log.warning("Cancelled connection to " +
                              str(remote_addr) + ".")
            await writer.drain()
            return None
        finally:
            writer.close()

    async def handle_ping(self, reader: asyncio.StreamReader,
                          writer: asyncio.StreamWriter, req: Request):
        """Handles a ping request and sends a pong back."""
        remote_addr, remote_port = writer.get_extra_info('peername')
        self._log.debug("Received Ping request from {:s}:{:d}.".format(remote_addr, remote_port))
        pong = Pong(nonce=req.nonce)
        writer.write(pong.pack())

    async def handle_pull(self, reader: asyncio.StreamReader,
                          writer: asyncio.StreamWriter, req: Request):
        """Handles a pull request and sends the local view from brahms back to the client."""
        remote_addr, remote_port = writer.get_extra_info('peername')
        self._log.debug("Received PullRequest from {:s}:{:d}.".format(remote_addr, remote_port))
        if self.brahms is None:
            # The brahms task did not started yet, so we cannot replay with
            # anything yet
            return

        pr = PullReply(nonce=req.nonce, peers=list(
            self.brahms.local_view_iter()))
        writer.write(pr.pack())

    async def handle_push(self, reader: asyncio.StreamReader,
                          writer: asyncio.StreamWriter, req: Request):
        """Handles a push request. It issues a challenge as proof of work and if correct the pushed peer is accepted."""
        remote_addr, remote_port = writer.get_extra_info('peername')
        self._log.debug(
            "Received PushRequest from {:s}:{:d}.".format(remote_addr,
                                                          remote_port))

        pc = PushChallenge(nonce=req.nonce, resource=urandom(8), difficulty=20)
        writer.write(pc.pack())

        pushr = PushReply()
        pushr_data_h = await reader.read(pushr.header_size)

        pushr.unpack_header(pushr_data_h, offset=0)
        pushr_data_r = await reader.read(pushr.size - pushr.header_size)

        pushr.unpack_reply(pushr_data_r, offset=0)

        if hashcash_check(pushr.answer.decode('ISO-8859-1'),  # type: ignore
                          resource=pc.resource.decode('ISO-8859-1'),
                          nbits=pc.difficulty) is False:
            msg = ("Received PushReply on connection to {:s}:{:d} but with "
                   "wrong challenge answer".format(remote_addr, remote_port))
            self._log.warning(msg)
            # reject and throw away the pushed peer

        else:
            self._log.debug("Received PushReply from {:s}:{:d} with correct "
                            "answer!".
                            format(remote_addr, remote_port))
            # accepts the pushed peer
            self.pushed_server.append(pushr.peer)

    def close(self) -> None:
        """Closes the server and must be called always at the end of the program."""
        self._log.info("closing...")
        self.__server.close()

    def wait_closed(self) -> None:
        """The loop can wait (block) with this method until close() is finished before terminating the program."""
        self._loop.run_until_complete(self.__server.wait_closed())
