import asyncio
from ipaddress import IPv4Address, IPv6Address
from logging import critical, debug, error, info, warning
from typing import List, Optional, Union, cast
from warnings import warn

from p2psec.rps.globals import DERCert, IPAddress, Port
from p2psec.rps.p2p.hashcash import hashcash_mint
from p2psec.rps.p2p.p2p_protocol import (
    Pong, Request, RequestType, RPSMsgFixedSize)
from p2psec.rps.p2p.p2p_pull import PullReply
from p2psec.rps.p2p.p2p_push import PushChallenge, PushReply
from p2psec.rps.p2p.peer_protocol import PeerProtocol
from p2psec.rps.peer import Peer

async def ping(ip: IPAddress, port: Port,
               loop: asyncio.AbstractEventLoop) -> None:
    """Sends a 'ping' request packet to the P2PServer and expects
        to receive a 'pong' from it. After that, checks if the
        nonces correspond to each other.

    Args:
        ip: IP address where the server is listening.
        port: Port where the server is listening.
        loop: The event loop in which the client runs.
    """
    debug("Send ping request to {:s}:{:d}.".format(str(ip), port))
    reader, writer = await asyncio.open_connection(str(ip), port, loop=loop)

    try:
        req = Request(request_type=RequestType.PING)
        req_packed = req.pack()

        writer.write(req_packed)

        while True:

            pong_data = await reader.read(RPSMsgFixedSize.PONG)

            pong = Pong()
            pong.unpack(pong_data, offset=0)

            if pong.nonce != req.nonce:
                msg = ("Received pong on connection to {:s}:{:d} but with "
                       "wrong nonce".format(str(ip), port))
                error(msg)
                warn(msg)
            else:
                debug("Received pong from {:s}:{:d}.".format(str(ip), port))
                break
    finally:
        writer.close()


async def pull(ip: IPAddress, port: Port,
               loop: asyncio.AbstractEventLoop) -> List[PeerProtocol]:
    """Sends a PullRequest packet to the P2PServer and expects
        to receive a PullReply one from it, containing the
        peers from its localview. After that, checks
        if the nonces correspond to each other.

    Args:
        ip: IP address where the server is listening.
        port: Port where the server is listening.
        loop: The event loop in which the client runs.

    Returns:
        A list containing the peers from sender's localview.
    """
    reader, writer = await asyncio.open_connection(str(ip), port, loop=loop)

    try:
        req = Request(request_type=RequestType.PULL)
        req_packed = req.pack()

        debug("Send pull request to {:s}:{:d}.".format(str(ip), port))
        writer.write(req_packed)

        while True:
            pullr = PullReply()
            pullr_data_h = await reader.read(pullr.header_size)

            pullr.unpack_header(pullr_data_h, offset=0)
            pullr_data_p = await reader.read(pullr.size - pullr.header_size)

            pullr.unpack_peers(pullr_data_p, offset=0)

            if pullr.nonce != req.nonce:
                msg = ("Received PullReply on connection to {:s}:{:d} but "
                       "with wrong nonce".format(str(ip), port))
                error(msg)
                warn(msg)
            else:
                debug("Received PullReply from {:s}:{:d}.".format(str(ip),
                                                                  port))
                return pullr.peers
    finally:
        writer.close()


async def push(ip: IPAddress, port: Port, itself: PeerProtocol,
               loop: asyncio.AbstractEventLoop) -> None:
    """Sends a PushRequest packet to the P2PServer and expects
        to receive a PushChallenge from it containing a difficulty
        and a random string. After that, checks if the nonces
        correspond to each other and use hashcash to 'mint'
        the string with a certain difficulty, in order to
        execute the proof of work. In the end, it sends a
        PushReply packet to the P2PServer with the answer
        for the given challenge and the data about itself.

    Args:
        ip: IP address where the server is listening.
        port: Port where the server is listening.
        loop: The event loop in which the client runs.
        itself: A PeerProtocol object representing the peer itself.
    """
    reader, writer = await asyncio.open_connection(str(ip), port, loop=loop)

    try:
        req = Request(request_type=RequestType.PUSH)
        req_packed = req.pack()

        debug("Send push request to {:s}:{:d}.".format(str(ip), port))
        writer.write(req_packed)

        while True:
            pushc = PushChallenge()
            pushc_data_h = await reader.read(pushc.header_size)

            pushc.unpack_header(pushc_data_h, offset=0)
            pushc_data_c = await reader.read(pushc.size - pushc.header_size)

            pushc.unpack_challenge(pushc_data_c, offset=0)

            if pushc.nonce != req.nonce:
                msg = ("Received PushChallenge on connection to {:s}:{:d} but"
                       " with wrong nonce".format(str(ip), port))
                error(msg)
                warn(msg)
            else:
                debug("Received PushChallenge from {:s}:{:d}.".format(str(ip),
                                                                      port))

                answer = hashcash_mint(  # type: ignore
                    pushc.resource.decode('ISO-8859-1'),
                    pushc.difficulty)

                pushr = PushReply(nonce=req.nonce,
                                  answer=answer.encode('ISO-8859-1'),
                                  peer=itself)

                writer.write(pushr.pack())
                break
    finally:
        writer.close()
