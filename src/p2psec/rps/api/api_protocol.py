import logging
from abc import ABCMeta, abstractclassmethod
from enum import IntEnum
from ipaddress import IPv4Address, IPv6Address
from struct import Struct
from typing import Optional, Tuple, cast
from warnings import warn

from p2psec.msg_types import (
    MAX_MSG_SIZE, MIN_MSG_SIZE, GossipValidationFlags, MsgFixedSize,
    MsgMinSize, MsgType, RPSPeerFlags)
from p2psec.rps.globals import (
    Automatic, BytesSize, BytesType, DERCert, IPAddress, Port)
from p2psec.rps.peer import Peer


class APIProtocol(metaclass=ABCMeta):
    """Base Class for all protocols used in the API of the RPS module.

    Handles the size and the type field, which all have in common.
    """
    size_type_struct = Struct("!HH")

    def __init__(self, size: Optional[BytesSize] = Automatic,
                 api_type: MsgType = None) -> None:
        """Initializes the first two fields size and type.

        Args:
            size: The value in the size field. If Automatic it will be
            calculated (see APIProtocol.size()). Automatic is recommended.
            api_type: The type of the message stored in the type field.
        """
        self._size = None  # type: Optional[BytesSize]
        self.size = size  # type: Optional[BytesSize]
        self.api_type = api_type
        self.log = logging.getLogger("APIProtocol")

    # Those two classes must be implemented in subclasses
    # (@abstractclassmethod can't be uses with @property)

    # @abstractclassmethod
    @property
    def size(self) -> BytesSize:
        """Calculates the size of the packet.

        Returns:
            if size is Automatic: The total size will be calculated based on the
            other attributes. Maybe returns a None if the size is not calculable
            cause too many attributes are not set.
            if size is an int: It returns just the size field, but throws a
            warning if the calculated size do not match the size field.
        """
        pass

    # @abstractclassmethod
    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def _set_size(self, value: Optional[BytesSize]) -> None:
        """Set the size field.

        Use APIProtocol.size instead of this method. This method is just
        required, because setter cannot be inherited.
        """
        if value is not Automatic and not (
                    MIN_MSG_SIZE <= value < MAX_MSG_SIZE):
            raise ValueError("size must be <= " + str(MIN_MSG_SIZE) +
                             " and < " + str(MAX_MSG_SIZE))
        self._size = value

    @classmethod
    def _unpack_size_type(cls, buffer: BytesType) -> Tuple[BytesSize, int]:
        return cast(Tuple[BytesSize, int],
                    cls.size_type_struct.unpack_from(buffer, offset=0))

    @classmethod
    def unpack_factory(cls, buffer: BytesType) -> "APIProtocol":
        """Unpacks the size and type field and return the corresponding object.

        The unpack() method of the subclasses must be still called to fully
        unpack the package. But before that the buffer has to be increased
        according to the size field.

        Args:
            buffer: A buffer of at least 8 bytes.

        Returns:
            A subclass of APIProtocol matching the type field. The size and type
            field are already unpacked and can be accessed.
        """
        size, api_type = APIProtocol._unpack_size_type(buffer)

        if api_type == MsgType.NSE_QUERY:
            msg = NSEQuery(size)  # type: APIProtocol
        elif api_type == MsgType.NSE_ESTIMATE:
            msg = NSEEstimate(size)
        elif api_type == MsgType.GOSSIP_ANNOUNCE:
            msg = GossipAnnounce(size)
        elif api_type == MsgType.GOSSIP_NOTIFY:
            msg = GossipNotify(size)
        elif api_type == MsgType.GOSSIP_NOTIFICATION:
            msg = GossipNotification(size)
        elif api_type == MsgType.GOSSIP_VALIDATION:
            msg = GossipValidation(size)
        elif api_type == MsgType.RPS_QUERY:
            msg = RPSQuery(size)
        elif api_type == MsgType.RPS_PEER:
            msg = RPSPeer(size=size)
        else:
            raise NotImplementedError("Message Type " + str(api_type) +
                                      " not supported yet.")

        return msg

    @abstractclassmethod
    def pack(self) -> BytesType:
        """Packs the object into bytes suitable for network transmission.

        Returns:
            Plain bytes in encoded according to the specification.
        """
        pass

    @abstractclassmethod
    def unpack(self, buffer: BytesType, offset: BytesSize) -> None:
        """Unpacks the bytes into the members of the object.

        Precondition:
            The size and type field must be set already. See unpack_factory().

        Postcondition:
            All members of the class must be set.

        Args:
            buffer: A buffer of length size - 8 bytes.
            offset: of the buffer
        """
        pass

    def __repr__(self) -> str:
        if isinstance(self.size, IntEnum):
            return (repr(self.__class__.__name__) + ": size = " + repr(int(
                self.size)) + " (" + repr(self.size) +
                    ") bytes, type = " + repr(self.api_type))
        else:
            return (repr(self.__class__.__name__) + ": size = " +
                    repr(int(self.size)) + " bytes, type = " +
                    repr(self.api_type))

    def __str__(self) -> str:
        header = "|{:<4d}{:>4d}|{:<4d}{:>4d}|{:<4d}{:>4d}|{:<4d}{:>4d}|".format(
            0, 7, 8, 15, 16, 23, 24, 31
        )
        sep = "|{:-^35s}|".format('')
        line = "|{:^17d}|{:^17s}|".format(self.size, self.api_type.name)
        return header + '\n' + sep + "\n" + str(line)


class NSEQuery(APIProtocol):
    def __init__(self, size: BytesSize = Automatic) -> None:
        super(NSEQuery, self).__init__(size, MsgType.NSE_QUERY)

    def pack(self) -> BytesType:
        return self.size_type_struct.pack(self.size, self.api_type.value)

    def unpack(self, buffer: BytesType, offset: BytesSize) -> None:
        raise NotImplementedError()

    @property
    def size(self) -> Optional[BytesSize]:
        if self._size and self._size != MsgFixedSize.NSE_QUERY:
            msg = ("Calculated size {:d} do not match size specified "
                   "{:d}!".format(self._size, MsgFixedSize.NSE_QUERY))
            self.log.error(msg)
            warn(msg)

        if self._size is not Automatic:
            return self._size
        else:
            return self.size_type_struct.size

    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def __repr__(self) -> str:
        return super(NSEQuery, self).__repr__()

    def __str__(self) -> str:
        return super(NSEQuery, self).__str__()


class NSEEstimate(APIProtocol):
    estimate_peers_struct = Struct("!I")
    estimate_std_dev_struct = Struct("!I")

    def __init__(self, size: BytesSize = Automatic,
                 estimate_peers: Optional[int] = None,
                 estimate_std_dev: Optional[int] = None) -> None:
        super(NSEEstimate, self).__init__(size, MsgType.NSE_ESTIMATE)

        self.estimate_peers = estimate_peers
        self.estimate_std_dev = estimate_std_dev

    def pack(self) -> BytesType:
        buffer = bytearray(self.size)

        pos = 0
        self.size_type_struct.pack_into(buffer, pos, self.size,
                                        self.api_type.value)
        pos += self.size_type_struct.size
        self.estimate_peers_struct.pack_into(buffer, pos, self.estimate_peers)

        pos += self.estimate_peers_struct.size
        self.estimate_std_dev_struct.pack_into(buffer, pos,
                                               self.estimate_std_dev)

        return buffer

    def unpack(self, buffer: BytesType, offset: BytesSize) -> None:
        pos = offset
        self.estimate_peers = self.estimate_peers_struct. \
            unpack_from(buffer, offset=pos)[0]

        pos += self.estimate_peers_struct.size

        self.estimate_std_dev = self.estimate_std_dev_struct. \
            unpack_from(buffer, offset=pos)[0]

    @property
    def size(self) -> Optional[BytesSize]:

        size = self.size_type_struct.size
        size += self.estimate_peers_struct.size
        size += self.estimate_std_dev_struct.size

        if self._size and self._size != size:
            msg = ("Calculated size {:d} do not match size specified "
                   "{:d}!".format(size, self._size))
            self.log.error(msg)
            warn(msg)

        if self._size is not Automatic:
            return self._size
        else:
            return size

    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def __repr__(self) -> str:
        line = ",\npeers = " + str(self.estimate_peers) + ", std dev = " + \
               str(self.estimate_std_dev)

        return super(NSEEstimate, self).__repr__() + line

    def __str__(self) -> str:
        line = "|{:^17s}|{:^17s}|".format(str(self.estimate_peers),
                                          str(self.estimate_std_dev))

        return super(NSEEstimate, self).__str__() + "\n" + line


class GossipAnnounce(APIProtocol):
    ttl_struct = Struct("!B")
    reserved_struct = Struct("!B")
    data_type_struct = Struct("!H")

    def __init__(self, size: BytesSize = Automatic,
                 ttl: Optional[int] = None,
                 reserved: Optional[int] = 0,
                 data_type: Optional[int] = None,
                 data: Optional[BytesType] = None) -> None:
        super(GossipAnnounce, self).__init__(size, MsgType.GOSSIP_ANNOUNCE)

        self.ttl = ttl
        self.reserved = reserved
        self.data_type = data_type
        self.data = data

    def pack(self) -> BytesType:
        buffer = bytearray(self.size)

        pos = 0
        self.size_type_struct.pack_into(buffer, pos, self.size,
                                        self.api_type.value)
        pos += self.size_type_struct.size
        self.ttl_struct.pack_into(buffer, pos, self.ttl)

        pos += self.ttl_struct.size
        self.reserved_struct.pack_into(buffer, pos, self.reserved)

        pos += self.reserved_struct.size
        self.data_type_struct.pack_into(buffer, pos, self.data_type)

        pos += self.data_type_struct.size
        buffer[pos: pos + len(self.data)] = self.data

        return buffer

    def unpack(self, buffer: BytesType, offset: BytesSize) -> None:
        raise NotImplementedError()

    @property
    def size(self) -> Optional[BytesSize]:

        is_calculable = True

        size = self.size_type_struct.size
        size += self.ttl_struct.size
        size += self.reserved_struct.size
        size += self.data_type_struct.size

        if self.data is None:
            msg = "Data field is not set. Unable to calculate size!"
            self.log.error(msg)
            warn(msg)
            is_calculable = False
        else:
            size += len(self.data)

        if is_calculable and self._size and self._size != size:
            msg = ("Calculated size {:d} do not match size specified "
                   "{:d}!".format(size, self._size))
            warn(msg)
            self.log.error(msg)

        if self._size is not Automatic:
            return self._size
        elif is_calculable:
            return size
        else:
            msg = ("Unable to calculate the size or the size attribute is not "
                   "set.")
            self.log.error(msg)
            warn(msg)
            return None

    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def __repr__(self) -> str:
        line = ",\nttl = " + str(self.ttl) + ", reserved = " + \
               str(self.reserved) + ", data type = " + str(self.data_type) \
               + ", data = " + str(self.data)

        return super(GossipAnnounce, self).__repr__() + line

    def __str__(self) -> str:
        line = "|{:^8s}|{:^8s}|{:^17s}|".format(str(self.ttl),
                                                str(self.reserved),
                                                str(self.data_type))

        line += "\n" + "|{:^35s}|".format(str(self.data))

        return super(GossipAnnounce, self).__str__() + "\n" + line


class GossipNotify(APIProtocol):
    reserved_struct = Struct("!H")
    data_type_struct = Struct("!H")

    def __init__(self, size: BytesSize = Automatic,
                 reserved: Optional[int] = 0,
                 data_type: Optional[int] = None) -> None:
        super(GossipNotify, self).__init__(size, MsgType.GOSSIP_NOTIFY)

        self.reserved = reserved
        self.data_type = data_type

    def pack(self) -> BytesType:
        buffer = bytearray(self.size)

        pos = 0
        self.size_type_struct.pack_into(buffer, pos, self.size,
                                        self.api_type.value)
        pos += self.size_type_struct.size
        self.reserved_struct.pack_into(buffer, pos, self.reserved)

        pos += self.data_type_struct.size
        self.data_type_struct.pack_into(buffer, pos, self.data_type)

        return buffer

    def unpack(self, buffer: BytesType, offset: BytesSize) -> None:
        raise NotImplementedError()

    @property
    def size(self) -> Optional[BytesSize]:

        size = self.size_type_struct.size
        size += self.reserved_struct.size
        size += self.data_type_struct.size

        if self._size and self._size != size:
            msg = ("Calculated size {:d} do not match size specified "
                   "{:d}!".format(size, self._size))
            self.log.error(msg)
            warn(msg)

        if self._size is not Automatic:
            return self._size
        else:
            return size

    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def __repr__(self) -> str:
        line = ",\nreserved = " + str(self.reserved) + ", data type = " \
               + str(self.data_type)

        return super(GossipNotify, self).__repr__() + line

    def __str__(self) -> str:
        line = "|{:^17s}|{:^17s}|".format(str(self.reserved),
                                          str(self.data_type))

        return super(GossipNotify, self).__str__() + "\n" + line


class GossipNotification(APIProtocol):
    message_id_struct = Struct("!H")
    data_type_struct = Struct("!H")

    def __init__(self, size: BytesSize = Automatic,
                 message_id: Optional[int] = None,
                 data_type: Optional[int] = None,
                 data: Optional[bytes] = None) -> None:
        super(GossipNotification, self).__init__(size,
                                                 MsgType.GOSSIP_NOTIFICATION)

        self.message_id = message_id
        self.data_type = data_type
        self.data = data

    def pack(self) -> BytesType:
        buffer = bytearray(self.size)

        pos = 0
        self.size_type_struct.pack_into(buffer, pos, self.size,
                                        self.api_type.value)
        pos += self.size_type_struct.size
        self.message_id_struct.pack_into(buffer, pos, self.message_id)

        pos += self.message_id_struct.size
        self.data_type_struct.pack_into(buffer, pos, self.data_type)

        pos += self.data_type_struct.size
        buffer[pos: pos + len(self.data)] = self.data

        return buffer

    def unpack(self, buffer: BytesType, offset: BytesSize) -> None:
        pos = offset
        self.message_id = self.message_id_struct. \
            unpack_from(buffer, offset=pos)[0]

        pos += self.message_id_struct.size

        self.data_type = self.data_type_struct. \
            unpack_from(buffer, offset=pos)[0]

        pos += self.data_type_struct.size

        self.data = buffer[pos:offset + self._size]

    @property
    def size(self) -> Optional[BytesSize]:

        size = self.size_type_struct.size
        is_calculable = True

        size += self.message_id_struct.size
        size += self.data_type_struct.size

        if self.data is None:
            msg = "Data field is not set. Unable to calculate size!"
            warn(msg)
            self.log.error(msg)
            is_calculable = False
        else:
            size += len(self.data)

        if self._size is not Automatic:
            return self._size
        elif is_calculable:
            return size
        else:
            msg = ("Unable to calculate the size or the size attribute is not "
                   "set.")
            self.log.error(msg)
            warn(msg)
            return None

    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def __repr__(self) -> str:
        line = ",\nmessage id = " + str(self.message_id) + ", data type = " + \
               str(self.data_type) + ", data = " + str(self.data)

        return super(GossipNotification, self).__repr__() + line

    def __str__(self) -> str:
        line = "|{:^17s}|{:^17s}|".format(str(self.message_id),
                                          str(self.message_id))

        line += "\n" + "|{:^35s}|".format(str(self.data))

        return super(GossipNotification, self).__str__() + "\n" + line


class GossipValidation(APIProtocol):
    message_id_struct = Struct("!H")
    flags_struct = Struct("!H")

    def __init__(self, size: BytesSize = Automatic,
                 message_id: Optional[int] = None,
                 flags: GossipValidationFlags = None) -> None:
        super(GossipValidation, self).__init__(size, MsgType.GOSSIP_VALIDATION)

        self.message_id = message_id
        self.flags = flags

    def pack(self) -> BytesType:
        buffer = bytearray(self.size)

        pos = 0
        self.size_type_struct.pack_into(buffer, pos, self.size,
                                        self.api_type.value)
        pos += self.size_type_struct.size
        self.message_id_struct.pack_into(buffer, pos, self.message_id)

        pos += self.message_id_struct.size
        self.flags_struct.pack_into(buffer, pos, self.flags)

        return buffer

    def unpack(self, buffer: BytesType, offset: BytesSize) -> None:
        raise NotImplementedError()

    @property
    def size(self) -> Optional[BytesSize]:

        size = self.size_type_struct.size
        size += self.message_id_struct.size
        size += self.flags_struct.size

        if self._size and self._size != size:
            msg = ("Calculated size {:d} do not match size specified "
                   "{:d}!".format(size, self._size))
            warn(msg)
            self.log.error(msg)

        if self._size is not Automatic:
            return self._size
        else:
            return size

    @size.setter  # type: ignore
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def __repr__(self) -> str:
        line = ",\nmessage id = " + str(self.message_id) + ", flags = " + \
               str(self.flags)

        return super(GossipValidation, self).__repr__() + line

    def __str__(self) -> str:
        line = "|{:^17s}|{:^17s}|".format(str(self.message_id),
                                          str(self.flags))

        return super(GossipValidation, self).__str__() + "\n" + line


class RPSQuery(APIProtocol):
    def __init__(self, size: BytesSize = MsgFixedSize.RPS_QUERY) -> None:
        super(RPSQuery, self).__init__(size, MsgType.RPS_QUERY)

    @property
    def size(self) -> Optional[BytesSize]:
        if MsgFixedSize.RPS_QUERY != self._size:
            warn("Specified size {:d} do not match the value of this fixed "
                 "size Protocol {:d}!".format(self._size,
                                              MsgFixedSize.RPS_QUERY))
        if self._size is not Automatic:
            return self._size
        else:
            return self.size_type_struct.size

    @size.setter
    def size(self, value: Optional[BytesSize]) -> None:
        self._set_size(value)

    def pack(self) -> BytesType:
        return self.size_type_struct.pack(self.size, self.api_type.value)

    def unpack(self, buffer: BytesType, offset: BytesSize):
        raise NotImplementedError()


class RPSPeer(APIProtocol):
    port_ipv_struct = Struct("!HH")
    ipv4_struct = Struct("!L")
    ipv6_struct = Struct("!16p")

    def __init__(self, peer: Optional[Peer] = None,
                 size: BytesSize = Automatic, port: Port = None,
                 ipv6: Optional[bool] = Automatic, ip: IPAddress = None,
                 hostkey: DERCert = None) -> None:
        self._ipv = None  # type: Optional[bool]
        self.port = None
        self.ip = None
        self.hostkey = None

        if peer is not None:
            self.port = peer.onion_port
            self.ip = peer.ip
            self.hostkey = peer.hostkey

        super(RPSPeer, self).__init__(size, MsgType.RPS_PEER)
        if port is not None:
            self.port = port
        self.ipv6 = ipv6
        if ip is not None:
            self.ip = ip
        if hostkey is not None:
            self.hostkey = hostkey

    @property
    def peer(self) -> Optional[Peer]:
        if self.hostkey is not None:
            return Peer(ip=self.ip, onion_port=self.port, rps_port=None,
                        hostkey=self.hostkey)
        else:
            return None

    @peer.setter
    def peer(self, value: Peer) -> None:
        self.ip = value.ip
        self.port = value.onion_port
        self.hostkey = value.hostkey

    @property
    def ipv6(self) -> Optional[bool]:
        detected = None

        if self.ip:
            if self.ip.version == 4:
                detected = False
            elif self.ip.version == 6:
                detected = True
            else:
                raise NotImplementedError("Other IP versions then v4 or v6 are "
                                          "not supported.")

        if self._ipv is not Automatic:
            if self._ipv != detected:
                warn("IP version 'ipv' boolean do not match the ip version of "
                     "the attribute 'ip'!")
            return self._ipv
        elif self.ip is None:
            warn("IP address is not set yet. Unable to detect version!")
            return None
        else:
            return detected

    @ipv6.setter
    def ipv6(self, value: bool) -> None:
        self._ipv = value

    @property
    def size(self) -> Optional[BytesSize]:
        is_calculable = True

        size = self.size_type_struct.size
        size += self.port_ipv_struct.size
        if self.ipv6 is None:
            msg = ("IP version not set or detectable. Size could not be "
                   "calculated!")
            warn(msg)
            self.log.error(msg)
            is_calculable = False
        elif not self.ipv6:  # IPv4
            size += self.ipv4_struct.size
        else:  # IPv6
            size += self.ipv6_struct.size

        if self.hostkey is None:
            msg = "Hostkey is not set. Unable to calculate size!"
            warn(msg)
            self.log.error(msg)
            is_calculable = False
        else:
            size += len(self.hostkey)

        if is_calculable and self._size and self._size != size:
            msg = ("Calculated size {:d} do not match size specified "
                   "{:d}!".format(size, self._size))
            warn(msg)
            self.log.error(msg)

        if self._size is not Automatic:
            return self._size
        elif is_calculable:
            return size
        else:
            return None

    @size.setter
    def size(self, value: BytesSize) -> None:
        self._set_size(value)

    def pack(self) -> BytesType:
        buffer = bytearray(self.size)

        pos = 0
        self.size_type_struct.pack_into(buffer, pos, self.size,
                                        self.api_type.value)
        pos += self.size_type_struct.size
        self.port_ipv_struct.pack_into(buffer, pos,
                                       self.port, self.flags)

        pos += self.port_ipv_struct.size
        ip_packed = self.ip.packed
        buffer[pos: pos + len(ip_packed)] = ip_packed

        pos += len(ip_packed)
        buffer[pos: pos + len(self.hostkey)] = self.hostkey

        return buffer

    def unpack(self, buffer: BytesType, offset: BytesSize) -> None:
        pos = offset
        self.port, iflags = self.port_ipv_struct.unpack_from(buffer, offset=pos)

        # ignore all not supported flags
        iflags = iflags & RPSPeerFlags.ipv6
        flags = RPSPeerFlags(iflags)

        pos += self.port_ipv_struct.size
        if RPSPeerFlags.ipv6 not in flags:
            self.ipv6 = False
            self.ip = IPv4Address(buffer[pos:pos + 4])
            pos += 4
        else:
            self.ipv6 = True
            self.ip = IPv6Address(buffer[pos:pos + 16])
            pos += 6

        self.hostkey = DERCert(buffer[pos:offset + self._size])

    @property
    def flags(self) -> RPSPeerFlags:
        flags = RPSPeerFlags(0)
        if self.ipv6:
            flags |= RPSPeerFlags.ipv6

        return flags

    def __str__(self) -> str:
        sflags = str(self.flags)
        sflags = sflags.replace("RPSPeerFlags.", '')

        line1 = "|{:^17d}|{:^17s}|".format(self.port, sflags)
        line2 = "|{:^35s}|".format(str(self.ip))
        line3 = "|{:^35s}|".format("0x" + str(self.hostkey[0:15].hex()) + "...")
        return (super(RPSPeer, self).__str__() + '\n' + str(line1) + '\n' +
                str(line2)) + '\n' + str(line3)

    def __repr__(self) -> str:
        return (super(RPSPeer, self).__repr__() + ", port = " +
                repr(self.port) + ", ipv6 = " + repr(self.ipv6) + ", flags = "
                + repr(self.flags) + ", ip = " + repr(self.ip) +
                ", DER-hostkey = 0x" + repr(self.hostkey.hex()))
