import asyncio
from ipaddress import IPv4Address
from logging import critical, debug, error, info, warning
from typing import Tuple, cast

from p2psec.msg_types import (MIN_MSG_SIZE, GossipValidationFlags,
                              MsgFixedSize, MsgMinSize, MsgType)
from p2psec.rps.api.api_protocol import (APIProtocol, GossipAnnounce,
                                         GossipNotify, GossipValidation,
                                         NSEEstimate, NSEQuery)
from p2psec.rps.globals import BytesType, DERCert, IPAddress, Port
from p2psec.rps.p2p.peer_protocol import PeerProtocol

async def nse_query(ip: IPAddress, port: Port, loop: asyncio.AbstractEventLoop) -> Tuple[int, int]:
    """Sends a NSEQuery packet to the APIServer (NSE) and
        receives a NSEEstimate packet containing the
        estimated data from the network.

    Args:
        ip: IP address where the server is listening.
        port: Port where the server is listening.
        loop: The event loop in which the client runs.

    Returns:
        A tuple containing the estimate peer and estimate
        standard deviation received by NSE module.
    """
    reader, writer = await asyncio.open_connection(str(ip), port, loop=loop)
    try:
        req = NSEQuery()
        req_packed = req.pack()

        debug("Send NSEQuery to {:s}:{:d}.".format(ip, port))
        writer.write(req_packed)

        while True:
            resp_data = await reader.read(MsgFixedSize.NSE_ESTIMATE)

            resp = cast(NSEEstimate, APIProtocol.unpack_factory(resp_data))
            resp.unpack(resp_data, offset=MIN_MSG_SIZE)

            debug("Received NSEEstimate from {:s}:{:d}.".format(ip, port))
            print(resp)
            break

        return resp.estimate_peers, resp.estimate_std_dev

    finally:
        writer.close()


async def gossip_announce(ip: IPAddress, port: Port, loop: asyncio.AbstractEventLoop,
                          data_type: int, ttl: int, data: BytesType) -> None:
    """Sends a GossipAnnounce packet to the APIServer (Gossip)
        containing a certain packet that we want to send to
        other peers in the data field.

    Args:
        ip: IP address where the server is listening.
        port: Port where the server is listening.
        loop: The event loop in which the client runs.
        data_type: The type of data we want to send.
        ttl: Number of hops required that data should be spread.
        data: The effective data we want to send.
    """
    reader, writer = await asyncio.open_connection(str(ip), port, loop=loop)

    try:
        req = GossipAnnounce(data_type=data_type, ttl=ttl, data=data)
        req_packed = req.pack()

        debug("Send GossipAnnounce to {:s}:{:d}.".format(ip, port))
        writer.write(req_packed)

    finally:
        writer.close()


async def gossip_notify(ip: IPAddress, port: Port, loop: asyncio.AbstractEventLoop,
                        data_type: int) -> None:
    """Sends a GossipNotify packet to the APIServer (Gossip),
        telling it that we want to receive data containing
        the type of data specified.

    Args:
        ip: IP address where the server is listening.
        port: Port where the server is listening.
        loop: The event loop in which the client runs.
        data_type: The type of data we want to receive.
    """
    reader, writer = await asyncio.open_connection(str(ip), port, loop=loop)

    try:
        req = GossipNotify(data_type=data_type)
        req_packed = req.pack()

        debug("Send GossipNotify to {:s}:{:d}.".format(ip, port))
        writer.write(req_packed)

    finally:
        writer.close()


async def notify_rps_hello(ip: IPAddress, port: Port, loop: asyncio.AbstractEventLoop) -> None:
    """Sends a GossipNotify packet to the APIServer (Gossip)
        with RPSHello data type.

    Args:
        ip: IP address where the server is listening.
        port: Port where the server is listening.
        loop: The event loop in which the client runs.
    """
    debug("Notify Gossip for RPSHello to {:s}:{:d}.".format(ip, port))

    await gossip_notify(ip, port, loop, MsgType.RPS_HELLO)


async def rps_hello(ip: IPAddress, port: Port, loop: asyncio.AbstractEventLoop,
                    itself: PeerProtocol) -> None:
    """Sends a GossipAnnounce packet to the APIServer (Gossip),
        with a RPSHello packet, containing a PeerProtocol
        object data.

    Args:
        ip: IP address where the server is listening.
        port: Port where the server is listening.
        loop: The event loop in which the client runs.
        itself: The data about the peer itself.
    """

    data = itself.pack()

    debug("Send Gossip a RPSHello to {:s}:{:d}.".format(ip, port))

    await gossip_announce(ip, port, loop, data_type=MsgType.RPS_HELLO, ttl=10, data=data)
