import asyncio
import logging
from concurrent.futures import CancelledError
from ipaddress import IPv4Address, IPv6Address, ip_address
from typing import Iterable, Optional, cast

from p2psec.msg_types import MIN_MSG_SIZE, MsgType
from p2psec.rps.api.api_protocol import (APIProtocol, DERCert,
                                         GossipNotification, GossipValidation,
                                         GossipValidationFlags, RPSPeer,
                                         RPSQuery)
from p2psec.rps.brahms import Brahms
from p2psec.rps.globals import Port
from p2psec.rps.p2p.peer_protocol import PeerProtocol
import p2psec.rps.p2p.client as p2p_client
from p2psec.rps.globals import DERCert, IPAddress, Port, rps_config


class APIServer(object):
    """Class for the APIServer.

    It is the server to connect between modules in the same peer.
    """

    def __init__(self, loop: asyncio.AbstractEventLoop,
                 listen: Iterable[str], port: Port, brahms: Optional[Brahms],
                 itself: PeerProtocol) -> None:
        """Initializes the server with loop, listen ip, port, brahms algorithm and a Peer Object.

        Args:
            loop: The event loop in which the server runs.
            listen: The IP address in which the server should listen on.
            port: The port in which the server should listen on.
            brahms:  A reference to brahms. It can be None if brahms is not started yet. Add it later via attribute.
            itself: A PeerProtocol object representing the peer itself.
        """
        self.itself = itself
        self.loop = loop
        self.log = logging.getLogger("API.Server")
        self.brahms = brahms
        coro = asyncio.start_server(self.handle_connection,
                                    cast(str, listen), port, loop=loop)
        self.log.info("started at " + ", ".join(listen) + " on port " +
                      str(port) + ".")
        self.server = loop.run_until_complete(coro)

    async def handle_connection(self, reader: asyncio.StreamReader,
                                writer: asyncio.StreamWriter) -> None:
        """Handles the connection established with the client and process its requests."""
        remote_ip, remote_port = writer.get_extra_info('peername')
        remote_addr = "{:s}:{:d}".format(str(remote_ip), remote_port)
        self.log.info("Connected to " + str(remote_addr))

        try:
            while True:
                data = await reader.read(MIN_MSG_SIZE)
                # data = data.decode("utf-8")

                if not data:
                    self.log.info("Remote " + str(remote_addr) +
                                  " has closed the connection.")
                    await writer.drain()
                    return None

                req = APIProtocol.unpack_factory(data)

                if req.api_type == MsgType.RPS_QUERY:
                    rpsq = cast(RPSQuery, req)  # type: RPSQuery
                    await self.handle_rps_query(reader, writer, rpsq)
                    break
                elif req.api_type == MsgType.GOSSIP_NOTIFICATION:

                    data = await reader.read(req.size - MIN_MSG_SIZE)
                    req.unpack(data, offset=4)
                    gn = cast(GossipNotification, req)  # type: GossipNotification
                    await self.handle_gossip_notification(reader, writer, gn)

        except CancelledError:
            self.log.warning("Cancelled connection to " +
                             str(remote_addr) + ".")
            await writer.drain()
            return None
        finally:
            writer.close()

    async def handle_rps_query(self, reader: asyncio.StreamReader,
                               writer: asyncio.StreamWriter, rpsq: RPSQuery) -> None:
        """Handles the client's RPSQuery request.

        Parses the received packet and sends a RPSPeer packet with
        a random peer in it after getting rand peer from Brahms algorithm.
        """
        if self.brahms is None:
            # brahms algorithm not started yet, so we cannot return anything
            return
        remote_addr = writer.get_extra_info('peername')
        self.log.debug(str(remote_addr) + ": Received RPSQuery: " + repr(rpsq))

        peer = self.brahms.rand_peer()

        rpsp = RPSPeer(port=peer.onion_port, ip=peer.ip, hostkey=peer.hostkey)

        self.log.debug(str(remote_addr) + ": Sending back RPSPeer: " + repr(rpsp))
        packed = rpsp.pack()

        writer.write(packed)

    async def handle_gossip_notification(self, reader: asyncio.StreamReader,
                                         writer: asyncio.StreamWriter,
                                         gn: GossipNotification) -> None:
        """Handles the client's GossipNotification request.

        Parses the received packet and sends a GossipValidation
        packet to Gossip to tell if the message received is
        well-formed or not, this is, if it receives a correct
        RPSHello packet from another peer.

        It also sends a PushRequest to the peer with its own data,
        in the case the message was well-formed.
        """
        remote_addr = writer.get_extra_info('peername')

        self.log.debug(str(remote_addr) + ": Received GossipNotification: " +
                       repr(gn))

        flags = GossipValidationFlags(0)

        if self.wellformed(gn):
            flags |= GossipValidationFlags.wellformed
            self.push_to_peer(gn)

        gv = GossipValidation(message_id=gn.message_id, flags=flags)
        gv_packed = gv.pack()

        self.log.debug(str(remote_addr) + ": Send GossipValidation: " +
                       repr(gv))

        writer.write(gv_packed)

    def wellformed(self, gn: GossipNotification) -> bool:
        """Checks if the received RPSHello is well-formed, by
            checking the message type and if the data is 'unpackable'
            as a Peer object.
        """
        form = True

        if gn.api_type != MsgType.RPS_HELLO:
            form = False

        try:
            peer = PeerProtocol()
            peer.unpack(gn.data)

        except IndexError or BufferError:
            form = False

        return form

    def push_to_peer(self, gn: GossipNotification):
        """In the case that the message was well-formed, sends
            PushRequest to the sending peer.
        """
        peer = PeerProtocol()
        peer.unpack(gn.data)

        p2p_client.push(peer.ip, peer.rps_port, self.itself, self.loop)

    def close(self) -> None:
        """Closes the server and must be called always at the end of the program."""
        self.log.info("closing...")
        self.server.close()

    def wait_closed(self) -> None:
        """The loop can wait (block) with this method until close() is finished before terminating the program."""
        self.loop.run_until_complete(self.server.wait_closed())
