"""Implementation of the Brahms algorithm for RPS."""
import asyncio
import itertools
import logging
import math
import random
from asyncio import Future
from concurrent.futures import CancelledError
from typing import (
    AbstractSet, Any, Awaitable, Iterator, List, Optional, Sequence, Tuple,
    Union, cast)

import p2psec.rps.p2p.client as p2p_client
import p2psec.rps.api.client as api_client
from p2psec.rps.globals import DERCert
from p2psec.rps.p2p.peer_protocol import PeerProtocol
from p2psec.rps.peer import Peer
from p2psec.rps.sampler import Sampler, SamplerBlock
from p2psec.rps.globals import IPAddress, Port


class Brahms(object):
    """Implementation of the Brahms algorithm for RPS.

    heartbeat() and brahms() must run as two tasks in the same loop.
    """

    def __init__(self, loop: asyncio.AbstractEventLoop,
                 pushed_proportion: float, pulled_proportion: float,
                 history_proportion: float, network_size: int,
                 ping_timeout: int, heartbeat_round_time: int,
                 initial_peers: List[PeerProtocol],
                 pushed_server: List[PeerProtocol],
                 brahms_round_time: int, itself: PeerProtocol,
                 nse_ip: Optional[IPAddress], nse_port: Port) -> None:
        """Creates a brahms control unit.

        Args:
            loop: An event loop.
            pushed_proportion: Percentage of the peers kept in local view from pushed message to this server.
            pulled_proportion: Percentage of the peers kept in local view from pulled message from this server.
            history_proportion: Percentage of the peers copied from the sampler.
            network_size: An initial estimation of the network size, Should be much larger then the initial peers.
            ping_timeout: Time to wait afer a peer is marked as dead. (seconds)
            heartbeat_round_time: The minimum time until the nodes are checked again if they are still alive. (seconds).
            initial_peers: A list of initial peers to start with pull and pushes.
            pushed_server: A list shared with the P2PServer where received pushed can be saved and exchaged with here.
            brahms_round_time: The minimum time the brahms algorithm wait until he expects a responce from all set
                push and pulls. Therefore its the minimum time a brahms rund needs. (seconds)
            itself: Information about the host, this programm is running on.
            nse_ip: IPAddress of the NSE module.
            nse_port: Port of the NSE module.
        """
        super().__init__()
        self._log = logging.getLogger("Brahms")

        if len(initial_peers) < 2:
            msg = "At least two initial peers for brahms are required!"
            self._log.critical(msg)
            raise ValueError(msg)

        if pushed_proportion + pulled_proportion + history_proportion != 1:
            msg = "Proportions of pushed, pulled and history must sum up to 1!"
            self._log.critical(msg)
            raise ValueError(msg)

        self.__secure_random = random.SystemRandom()
        self._loop = loop
        self._pushed_server = pushed_server
        """A sequence of successful pushed out P2PServer receives."""
        self._brahms_round_time = brahms_round_time
        self.itself = itself

        self._pushed_proportion = pushed_proportion
        self._pushed = []  # type: List[PeerProtocol]
        """List for the pushed peers."""

        self._pulled_proportion = pulled_proportion
        self._pulled = []  # type: List[PeerProtocol]
        """List for the pulled peers."""

        self._history_proportion = history_proportion
        self._history = []  # type: List[PeerProtocol]
        """List for the history peers from sampler."""

        self._init_network_size = network_size
        self._nse_ip = nse_ip
        self._nse_port = nse_port

        # we assume that at least 1/3 peers are good behaving
        good = math.ceil(network_size / 3.0)
        # the minimum size pushed/pulled should have
        min_size = max(1, math.ceil(good ** (1. / 3.)))
        self._sampler = Sampler(min_size, attribute="hostkey")

        self._ping_timeout = ping_timeout
        self._heartbeat_round_time = heartbeat_round_time

        self._sampler.update_samplers(initial_peers)

        push_pulled_prop = self._pushed_proportion + self._pulled_proportion

        # split the initial peers into pushed and pulled
        pusheds = max(1, int((self._pushed_proportion / push_pulled_prop) *
                             len(initial_peers)))
        pulleds = max(1, int((self._pulled_proportion / push_pulled_prop) *
                             len(initial_peers)))
        # calculate the size of the history part of the local view
        _, _, hists = self._local_view_sizes_from_minimum(
            pusheds, pulleds)

        self._pushed.extend(initial_peers[0:pusheds])
        self._pulled.extend(initial_peers[pusheds:pusheds + pulleds])

        self._history.extend(self._sampler.rand_samples(hists))
        for s, i in zip(self._sampler, range(hists)):  # type: ignore
            self._history[i] = s.sample

    def _local_view_sizes_from_estimation(self, network_size: int) -> Tuple[int, int, int]:
        """Calculates the recommended local view propostion from a network size estimation.

        Args:
            network_size: An estimation of network size.

        Returns:
            The *absolute* sizes of the three parts of local view: pushed, pulled, history
        """
        # we assume that at least 1/3 peers are good behaving
        good = math.ceil(network_size / 3.0)
        # the minimum size pushed/pulled should have
        min_size = math.ceil(good ** (1. / 3.))

        # the smaller proportion will get the min size
        if self._pushed_proportion <= self._pulled_proportion:
            pushed_size = min_size
            pulled_size = math.ceil(
                (self._pulled_proportion / self._pushed_proportion) * min_size)
            history_size = math.ceil(
                (self._history_proportion / self._pushed_proportion) * min_size)
        else:
            pulled_size = min_size
            pushed_size = math.ceil(
                (self._pushed_proportion / self._pulled_proportion) * min_size)
            history_size = math.ceil(
                (self._history_proportion / self._pulled_proportion) * min_size)

        return max(1, pushed_size), max(1, pulled_size), max(1, history_size)

    def _local_view_sizes_from_minimum(self, pushed_size: int, pulled_size: int) -> Tuple[int, int, int]:
        """Calculates the recommended local view propostion from given pushed an pulled size.

        The input of this mehtod is usually the received pushes and pulles and therefore the known peers.
        Note that one of the sizes of pushed or pulled can decrease to enforce the specificed proportions.

        Args:
            pushed_size: The known peers pushed to this server,
            pulled_size: The known peers pulled from this server.

        Returns:
            The *absolute* sizes of the three parts of local view: pushed, pulled, history
        """
        # Look which is the minimum size of the local view so that we can still
        # enforce the specified proportions
        min_lv_size = min(math.ceil(pushed_size / self._pushed_proportion),
                          math.ceil(pulled_size / self._pulled_proportion))

        return (max(1, math.ceil(self._pushed_proportion * min_lv_size)),
                max(1, math.ceil(self._pulled_proportion * min_lv_size)),
                max(1, math.ceil(self._history_proportion * min_lv_size)),
                )

    def _rand_choices(self, population: Union[Sequence[Any], AbstractSet[Any]], k: int) -> List:
        """Makes cryptographically secure k choices out of the population.

        Returns:
            A new list of k elements or if the population is smaller than k, the population in a new list.
        """
        if len(population) <= k:
            return list(population)
        else:
            return self.__secure_random.sample(population, k)

    def rand_peer_choices(self, k: int) -> List[PeerProtocol]:
        """Makes cryptographically secure k peer choices from the local view.

        Returns:
            A new list of k peers or if the population is smaller than k, the population in a new list.
        """
        return self._rand_choices(
            self._pushed + self._pulled + self._history, k
        )

    def local_view_iter(self) -> Iterator[PeerProtocol]:
        """Iterator over local view."""
        return itertools.chain(self._pushed, self._pulled, self._history)

    @property
    def local_view_len(self) -> int:
        """Length of the local view."""
        return len(self._pushed) + len(self._pulled) + len(self._history)

    def rand_peer(self) -> PeerProtocol:
        # This multiple choices is just to prevent that the three lists have
        # to be concatenated (and therefore to create a new list) just to
        # select one random peer
        return self.__secure_random.choice(
            (self.__secure_random.choice(self._pushed),
             self.__secure_random.choice(self._pulled),
             self.__secure_random.choice(self._history)))

    async def heartbeat(self) -> None:
        """Pings regularily the peer in the sampler to check if they are still alive.

        SamplerBlocks with dead peers are reinitialized.
        """

        async def ping_sb(sampler_block: SamplerBlock) -> None:
            sample = cast(PeerProtocol, sampler_block.sample)
            self._log.debug("Send ping to {:s}:{:d}".format(str(sample.ip), sample.rps_port))

            try:
                # self._log.debug("wait on {:s}".format(str(sample)))
                await asyncio.wait_for(p2p_client.ping(sample.ip, sample.rps_port,
                                                       loop=self._loop),
                                       timeout=self._ping_timeout,
                                       loop=self._loop)

            except (ConnectionRefusedError, asyncio.TimeoutError):
                self._log.info("Reinitialize {:s} because no "
                               "response from {:s}:{:d}".format(str(sampler_block),
                                                                str(sample.ip), sample.rps_port))
                sampler_block.init()
            return

        try:
            while True:
                future_lst = list()

                # start ping procedure for every sample
                for sb in self._sampler:  # type: ignore
                    if sb.sample is None:
                        continue
                    ping_sb_fut = asyncio.ensure_future(ping_sb(sb),
                                                        loop=self._loop)
                    future_lst.append(ping_sb_fut)

                # start timer for the minimum time a round should have
                round_fut = asyncio.ensure_future(asyncio.sleep(
                    self._heartbeat_round_time),
                    loop=self._loop)  # type: Awaitable

                for fut in future_lst:
                    await fut

                # finally wait for the minimum time of a round
                self._log.info("All ping protocol runs terminated. Waiting "
                               "until next round begins. "
                               "{:s}".format(str(self._sampler)))
                await round_fut

        # for application teardown
        except CancelledError:
            self._log.info("Heartbeat cancelled.")
            return

    async def brahms(self) -> None:
        """Perform the actual brahms algorithm in rounds.

        Here push and pulls are sent, the local view is update as well as the sampler.
        """

        async def push_peer(peer_: Peer) -> None:
            """Do a push to a peer. Subfunction of brahms()"""

            try:
                await asyncio.wait_for(p2p_client.push(peer_.ip, peer_.rps_port,
                                                       itself=self.itself,
                                                       loop=self._loop),
                                       timeout=self._brahms_round_time,
                                       loop=self._loop)

            except (ConnectionRefusedError, asyncio.TimeoutError):
                pass

        async def pull_peer(peer_: Peer) -> List[PeerProtocol]:
            """Do a pull to a peer. Subfunction of brahms()"""

            try:
                res = await asyncio.wait_for(p2p_client.pull(peer_.ip,
                                                             peer_.rps_port,
                                                             loop=self._loop),
                                             timeout=self._brahms_round_time,
                                             loop=self._loop)
                return res
            except (ConnectionRefusedError, asyncio.TimeoutError):
                return []

        try:
            while True:
                # get estimation
                if self._nse_ip is None:
                    # fixed value for testing
                    (pusheds, pulleds, hists) = self._local_view_sizes_from_estimation(40)
                else:
                    estimation, _ = await api_client.nse_query(self._nse_ip, self._nse_port, loop=self._loop)
                    (pusheds, pulleds, hists) = self._local_view_sizes_from_estimation(estimation)

                # new round. Delete old pushes.
                del self._pushed_server[:]

                # starting all pushes
                push_futures = list()  # type: List[Future]
                push_dst = self.rand_peer_choices(pusheds)
                for peer in push_dst:
                    pf = asyncio.ensure_future(push_peer(peer),
                                               loop=self._loop)
                    push_futures.append(pf)

                # starting all pulles
                pull_futures = list()  # type: List[Future]
                pull_dst = self.rand_peer_choices(pulleds)
                for peer in pull_dst:
                    pf = asyncio.ensure_future(cast(Awaitable[None],
                                                    pull_peer(peer)),
                                               loop=self._loop)
                    pull_futures.append(pf)

                self._log.debug("Sent {:d} pushes and {:d} pulls to random "
                                "peers.".format(len(push_dst), len(pull_dst)))

                # give pull and pushes time to finish and limit the frequency of brahms rounds.
                await asyncio.sleep(self._brahms_round_time)

                # make sure the pushes are finished
                for pf in push_futures:
                    await pf

                # gather the results of pulls
                pulled_peers = list()  # type: List[PeerProtocol]
                for pf in pull_futures:
                    peers = await pf
                    pulled_peers.extend(peers)

                # update local view if we got enough pull and pushes (or not too much)
                if 0 < len(self._pushed_server) <= pusheds and 0 < len(
                    pulled_peers):
                    (pusheds, pulleds,
                     hists) = self._local_view_sizes_from_minimum(
                        len(self._pushed_server), len(pulled_peers)
                    )
                    self._pushed = self._rand_choices(self._pushed_server,
                                                      pusheds)
                    self._pulled = self._rand_choices(pulled_peers, pulleds)
                    self._history = self._sampler.rand_samples(hists)
                    self._log.debug("Update local view with {:d} pushes, {:d} pulles, and {:d} "
                                    "history.".format(len(self._pushed), len(self._pulled), len(self._history)))

                    self._sampler.amount = max(len(self._pushed),
                                               len(self._pulled))
                    self._sampler.update_samplers(
                        self._pushed_server + pulled_peers)
                else:
                    self._log.debug("Not enough push or pull received. Local view is not updated. pushes {:d}, "
                                    "pulles {:d}.".format(len(self._pushed_server), len(pulled_peers)))

        except CancelledError:
            self._log.info("Brahms cancelled.")
            return
