# P2PSEC Project - Summer Semester 2017

Building an anonymous and unobservable VoIP application using a P2P architecture

[![build status](https://gitlab.lrz.de/ga92qed/P2PSEC/badges/master/build.svg)](https://gitlab.lrz.de/ga92qed/P2PSEC/commits/master)
[![coverage report](https://gitlab.lrz.de/ga92qed/P2PSEC/badges/master/coverage.svg)](https://gitlab.lrz.de/ga92qed/P2PSEC/commits/master)
[![license](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://www.tldrlegal.com/l/gpl-3.0)
[![Maintenance](https://img.shields.io/maintenance/yes/2017.svg)]()

## Group 21 - RPS Module

* **Fabian Raab** <[fabian.raab@tum.de](mailto:Fabian Raab<fabian.raab@tum.de>)>

* **João Neto** <[neto@in.tum.de](mailto:Joao Neto<neto@in.tum.de>)>


Random Peer Sampling
====================

This application completely  implements an Random Peer Sampling (RPS) algorithm called *Brahms*:

Edward Bortnikov et al. “Brahms: Byzantine Resilient Random Membership Sampling”. In:
Proceedings of the Twenty-seventh ACM Symposium on Principles of Distributed Computing.
PODC ’08. New York, NY, USA: ACM, 2008, pp. 145–154. isbn: 978-1-59593-989-0. [doi:
10.1145/1400751.1400772](http://doi.acm.org/10.1145/1400751.1400772).



Building
========

The app require at least python 3.5.1.

Before you use the project, you should install the requirements:

    pip3 install -r requirements.txt

In the project folder:

    export PYTHONPATH=$(pwd)/src/

It is expected for all commands in this readme that you are in the project directory and have extended your python-path.

Running
=======

for help type:

    python3 -m p2psec.rps --help

To run the RPS module with the specified configuration `local_rps.toml`:

    python3 -m p2psec.rps -c misc/local_rps.toml -vvvv run

Or run it with no external communications (NSE and Gossip).
The NSE module is only required to estimate to how much peers push and pulls should be sent.
Here it is replaced by a static value of 40 (The RPS module works also with wrong estimations).
The RPS module need Gossip only for getting some initial peers of the network in the beginning.
So for testting the initial peers are replaced by the following three peers.
To test a small network of three peers run the three commands respectively on an own terminal on one host.
Start them in a timeframe of 8 seconds (or some initial peer could be marked immediately dead again):

client 1:

    python3 -m p2psec.rps -c misc/local_rps.toml --p2p-server-port=4441 -vvvv noexternal

client 2:

    python3 -m p2psec.rps -c misc/local_rps.toml --p2p-server-port=4442 -vvvv noexternal

client 3:

    python3 -m p2psec.rps -c misc/local_rps.toml --p2p-server-port=4443 -vvvv noexternal

Testing
=======

The static typed with python annotations. Static type checking can be done with:

    mypy ./src


This module is extensively tested. You can run the automatic unit tests via:

    pytest tests

To test them in a clean virtuel environment and on all supported python versions (make sure you have them installed),
 run:

    tox

It even passes security tests (hashcash is an external library ;-) ):

    bandit --recursive --confidence low ./src/
